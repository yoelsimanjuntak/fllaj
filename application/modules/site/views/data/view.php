<table class="table table-striped">
  <tbody>
    <tr>
      <td class="nowrap">Kategori</td><td>:</td>
      <td><?=$data[COL_JENIS]?></td>
    </tr>
    <tr>
      <td class="nowrap">Disposisi</td><td>:</td>
      <td><?=$data[COL_STATUS]?></td>
    </tr>
    <tr>
      <td class="nowrap">Status</td><td>:</td>
      <td><?=!empty($data[COL_STATUS_AKHIR])?$data[COL_STATUS_AKHIR]:'-'?></td>
    </tr>
    <tr>
      <td class="nowrap">Nama / NIK</td><td>:</td>
      <td><?=$data[COL_NAMA].' / '.$data[COL_NIK]?></td>
    </tr>
    <tr>
      <td class="nowrap">No. HP</td><td>:</td>
      <td><?=$data[COL_NO_HP]?></td>
    </tr>
    <tr>
      <td class="nowrap">Tempat & Tgl. Lahir</td><td>:</td>
      <td><?=$data[COL_TEMPAT_LAHIR].', '.date('d-m-Y', strtotime($data[COL_TGL_LAHIR])) ?></td>
    </tr>
    <tr>
      <td class="nowrap">Alamat</td><td>:</td>
      <td><?=$data[COL_ALAMAT].'<br />Kec. '.(!empty($data[COL_V_KECAMATAN])?$data[COL_V_KECAMATAN]:'-').'<br />Desa '.(!empty($data[COL_V_DESA])?$data[COL_V_DESA]:'-') ?></td>
    </tr>
    <tr>
      <td class="nowrap">Kondisi Medis</td><td>:</td>
      <td>
        <strong>Suhu Badan : </strong><?=!empty($data[COL_SUHU_BADAN])?$data[COL_SUHU_BADAN]:'-'?><br />
        <strong>Gejala : </strong><?=!empty($data[COL_KESEHATAN])?$data[COL_KESEHATAN]:'-'?><br />
        <strong>Catatan : </strong><?=!empty($data[COL_CATATAN_MEDIS])?$data[COL_CATATAN_MEDIS]:'-'?><br />
      </td>
    </tr>
    <tr>
      <td class="nowrap">Puskesmas Rujukan</td><td>:</td>
      <td><?=!empty($data[COL_V_PUSKESMAS])?$data[COL_V_PUSKESMAS]:'-'?></td>
    </tr>
    <tr>
      <td class="nowrap">Catatan Akhir</td><td>:</td>
      <td><?=!empty($data[COL_CATATAN_AKHIR])?$data[COL_CATATAN_AKHIR]:'-'?></td>
    </tr>
    <tr>
      <td class="nowrap">Timestamp</td><td>:</td>
      <td>
        <strong>Diinput : </strong><?=!empty($data[COL_TGL_INSERT])?date('d-m-Y H:i:s', strtotime($data[COL_TGL_INSERT])):'-'?><br />
        <strong>Diubah : </strong><?=!empty($data[COL_TGL_UPDATE])?date('d-m-Y H:i:s', strtotime($data[COL_TGL_UPDATE])):'-'?><br />
      </td>

    </tr>
  </tbody>
</table>
