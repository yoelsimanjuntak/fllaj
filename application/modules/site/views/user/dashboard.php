<?php
$ruser = GetLoggedUser();

 ?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
 </style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?=$title?></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <div class="card card-navy">
          <div class="card-header">
            <h5 class="card-title">PENGATURAN UMUM</h5>
          </div>
          <div class="card-body">
              <?php if (validation_errors()) { ?>
                  <div class="alert alert-danger">
                      <i class="fa fa-ban"></i> PESAN ERROR :
                      <ul>
                          <?= validation_errors() ?>
                      </ul>

                  </div>
              <?php } ?>

              <?php if (!empty($errormess)) { ?>
                  <div class="alert alert-danger">
                      <i class="fa fa-ban"></i> PESAN ERROR :
                      <?= $errormess ?>
                  </div>
              <?php } ?>

              <?php  if ($this->input->get('success')) { ?>
                  <div class="form-group alert alert-success alert-dismissible">
                      <i class="fa fa-check"></i>
                      Berhasil.
                  </div>
              <?php } ?>

              <?php  if ($this->input->get('error')) { ?>
                  <div class="form-group alert alert-danger alert-dismissible">
                      <i class="fa fa-ban"></i>
                      Gagal mengupdate data, silahkan coba kembali
                  </div>
              <?php } ?>

              <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
              <div class="form-group row">
                  <label  class="control-label col-sm-3">Nama Organisasi</label>
                  <div class="col-sm-9">
                      <input type="text" class="form-control" name="<?=SETTING_ORG_NAME?>" value="<?=!empty($data[SETTING_ORG_NAME]) ? $data[SETTING_ORG_NAME] : $this->setting_org_name?>" required />
                  </div>
              </div>
              <div class="form-group row">
                  <label  class="control-label col-sm-3">Alamat</label>
                  <div class="col-sm-9">
                      <textarea class="form-control" name="<?=SETTING_ORG_ADDRESS?>" rows="5" required><?=!empty($data[SETTING_ORG_ADDRESS]) ? $data[SETTING_ORG_ADDRESS] : $this->setting_org_address?></textarea>
                  </div>
              </div>
              <div class="form-group row">
                  <label  class="control-label col-sm-3">Telepon</label>
                  <div class="col-sm-9">
                      <input type="text" class="form-control" name="<?=SETTING_ORG_PHONE?>" value="<?=!empty($data[SETTING_ORG_PHONE]) ? $data[SETTING_ORG_PHONE] : $this->setting_org_phone?>" required />
                  </div>
              </div>
              <div class="form-group row">
                  <label  class="control-label col-sm-3">Fax</label>
                  <div class="col-sm-9">
                      <input type="text" class="form-control" name="<?=SETTING_ORG_FAX?>" value="<?=!empty($data[SETTING_ORG_FAX]) ? $data[SETTING_ORG_FAX] : $this->setting_org_fax?>" required />
                  </div>
              </div>
              <div class="form-group row">
                  <label  class="control-label col-sm-3">Email</label>
                  <div class="col-sm-9">
                      <input type="text" class="form-control" name="<?=SETTING_ORG_MAIL?>" value="<?=!empty($data[SETTING_ORG_MAIL]) ? $data[SETTING_ORG_MAIL] : $this->setting_org_mail?>" required />
                  </div>
              </div>
              <div class="form-group row">
                  <label  class="control-label col-sm-3">Facebook</label>
                  <div class="col-sm-9">
                      <input type="text" class="form-control" name="SETTING_ORG_FACEBOOK" value="<?=!empty($data['SETTING_ORG_FACEBOOK']) ? $data['SETTING_ORG_FACEBOOK'] : GetSetting('SETTING_ORG_FACEBOOK')?>" />
                  </div>
              </div>
              <div class="form-group row">
                  <label  class="control-label col-sm-3">Twitter</label>
                  <div class="col-sm-9">
                      <input type="text" class="form-control" name="SETTING_ORG_TWITTER" value="<?=!empty($data['SETTING_ORG_TWITTER']) ? $data['SETTING_ORG_TWITTER'] : GetSetting('SETTING_ORG_TWITTER')?>" />
                  </div>
              </div>
              <div class="form-group row">
                  <label  class="control-label col-sm-3">Instagram</label>
                  <div class="col-sm-9">
                      <input type="text" class="form-control" name="SETTING_ORG_INSTAGRAM" value="<?=!empty($data['SETTING_ORG_INSTAGRAM']) ? $data['SETTING_ORG_INSTAGRAM'] : GetSetting('SETTING_ORG_INSTAGRAM')?>" />
                  </div>
              </div>
              <div class="form-group row">
                  <button type="submit" class="btn btn-outline-secondary btn-block">SIMPAN</button>
              </div>
              <?=form_close()?>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-navy">
          <div class="card-header">
            <h5 class="card-title">PENGATURAN LANJUT</h5>
          </div>
          <div class="card-body">
            <?php if (validation_errors()) { ?>
                <div class="alert alert-danger">
                    <i class="fa fa-ban"></i> PESAN ERROR :
                    <ul>
                        <?= validation_errors() ?>
                    </ul>

                </div>
            <?php } ?>

            <?php if (!empty($errormess)) { ?>
                <div class="alert alert-danger">
                    <i class="fa fa-ban"></i> PESAN ERROR :
                    <?= $errormess ?>
                </div>
            <?php } ?>

            <?php  if ($this->input->get('success')) { ?>
                <div class="form-group alert alert-success alert-dismissible">
                    <i class="fa fa-check"></i>
                    Berhasil.
                </div>
            <?php } ?>

            <?php  if ($this->input->get('error')) { ?>
                <div class="form-group alert alert-danger alert-dismissible">
                    <i class="fa fa-ban"></i>
                    Gagal mengupdate data, silahkan coba kembali
                </div>
            <?php } ?>

            <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
            <div class="form-group row">
                <label  class="control-label col-sm-2">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="<?=SETTING_WEB_NAME?>" value="<?=!empty($data[SETTING_WEB_NAME]) ? $data[SETTING_WEB_NAME] : $this->setting_web_name?>" required />
                </div>
            </div>
            <div class="form-group row">
                <label  class="control-label col-sm-2">Description</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="<?=SETTING_WEB_DESC?>" value="<?=!empty($data[SETTING_WEB_DESC]) ? $data[SETTING_WEB_DESC] : $this->setting_web_desc?>" required />
                </div>
            </div>
            <div class="form-group row">
                <label  class="control-label col-sm-2">Version</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="<?=SETTING_WEB_VERSION?>" value="<?=!empty($data[SETTING_WEB_VERSION]) ? $data[SETTING_WEB_VERSION] : $this->setting_web_version?>" required />
                </div>
            </div>
            <div class="form-group row">
                <label  class="control-label col-sm-2">Logo</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="<?=SETTING_WEB_LOGO?>" value="<?=!empty($data[SETTING_WEB_LOGO]) ? $data[SETTING_WEB_LOGO] : $this->setting_web_logo?>" required />
                </div>
            </div>
            <div class="form-group row">
                <label  class="control-label col-sm-2">Preloader</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="<?=SETTING_WEB_PRELOADER?>" value="<?=!empty($data[SETTING_WEB_PRELOADER]) ? $data[SETTING_WEB_PRELOADER] : $this->setting_web_preloader?>" required />
                </div>
            </div>
            <div class="form-group row">
                <label  class="control-label col-sm-2">Skin Class</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="<?=SETTING_WEB_SKIN_CLASS?>" value="<?=!empty($data[SETTING_WEB_SKIN_CLASS]) ? $data[SETTING_WEB_SKIN_CLASS] : $this->setting_web_skin_class?>" required />
                </div>
            </div>
            <div class="form-group row">
                <label  class="control-label col-sm-2">DISQUS URL</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="<?=SETTING_WEB_DISQUS_URL?>" value="<?=!empty($data[SETTING_WEB_DISQUS_URL]) ? $data[SETTING_WEB_DISQUS_URL] : $this->setting_web_disqus_url?>" required />
                </div>
            </div>
            <div class="form-group row">
                <label  class="control-label col-sm-2">Footer Link</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="<?=SETTING_WEB_API_FOOTERLINK?>" value="<?=!empty($data[SETTING_WEB_API_FOOTERLINK]) ? $data[SETTING_WEB_API_FOOTERLINK] : $this->setting_web_api_footerlink?>" required />
                </div>
            </div>
            <div class="form-group row">
                <button type="submit" class="btn btn-outline-secondary btn-block">SIMPAN</button>
            </div>
            <?=form_close()?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
