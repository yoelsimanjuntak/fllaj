<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 30/01/2020
 * Time: 22:01
 */
$data = array();
$i = 0;
foreach ($res as $d) {
  $res[$i] = array(
      '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_UNIQ] . '" />',
      anchor('site/upload/edit/'.$d[COL_UNIQ],
      $d[COL_FILENAME],
      array(
        'class' => 'modal-popup-edit',
        'data-name' => $d[COL_FILENAME],
        'data-path' => MY_UPLOADURL.$d[COL_FILEPATH],
        'data-createdby' => /*$d[COL_CREATEDBY].' - '.*/$d['Nm_CreatedBy'],
        'data-createdon' => date('Y-m-d H:i:s', strtotime($d[COL_CREATEDON])),
        'data-updatedby' => /*$d[COL_UPDATEDBY].' - '.*/$d['Nm_UpdatedBy'],
        'data-updatedon' => date('Y-m-d H:i:s', strtotime($d[COL_UPDATEDON]))
      )),
      anchor(MY_UPLOADURL.$d[COL_FILEPATH], $d[COL_FILEPATH], array('target'=>'_blank')),
      $d[COL_CREATEDBY],
      date("Y-m-d H:i:s", strtotime($d[COL_CREATEDON]))
  );
  $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?> <small class="text-sm font-weight-light"> Data</small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <p>
                    <?=anchor('site/upload/delete','<i class="fa fa-trash"></i> HAPUS',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
                    <?=anchor('site/upload/add','<i class="fa fa-plus"></i> TAMBAH',array('class'=>'modal-popup btn btn-primary btn-sm'))?>
                </p>
                <div class="card card-default">
                    <div class="card-body">
                        <form id="dataform" method="post" action="#">
                            <table id="datalist" class="table table-bordered table-hover display compact">

                            </table>
                        </form>
                    </div>
                </div>
                <div class="modal fade" id="modal-editor" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Upload</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="text-danger error-message"></p>
                                <form id="form-editor" method="post" action="#" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Nama Dokumen</label>
                                        <input type="text" class="form-control" name="<?=COL_FILENAME?>" required />
                                    </div>
                                    <div class="form-group">
                                      <label>Upload</label>
                                      <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text"><i class="fad fa-image"></i></span>
                                        </div>
                                        <div class="custom-file">
                                          <input type="file" class="custom-file-input" name="userfile">
                                          <label class="custom-file-label" for="userfile">PILIH FILE</label>
                                        </div>
                                      </div>
                                    </div>
                                </form>
                                <p class="text-muted text-sm font-italic label-info mb-0 mt-3 pt-2"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">BATAL</button>
                                <button type="button" class="btn btn-outline-primary btn-ok">SIMPAN</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
  bsCustomFileInput.init();
  var dataTable = $('#datalist').dataTable({
    "autoWidth": false,
    //"sDom": "Rlfrtip",
    "aaData": <?=$data?>,
    //"bJQueryUI": true,
    //"aaSorting" : [[5,'desc']],
    "scrollY" : '44vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [[ 1, "asc" ]],
    "columnDefs": [
        { className: "dt-body-right nowrap", "targets": [ 4 ] },
        { className: "nowrap", "targets": [ 0,1,2,3,4 ] }
    ],
    "aoColumns": [
        {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />", "width": "10px","bSortable":false},
        {"sTitle": "Dokumen"},
        {"sTitle": "URL","bSortable":false},
        {"sTitle": "Diinput Oleh"},
        {"sTitle": "Diinput Pada"}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.modal-popup, .modal-popup-edit', $(row)).click(function(){
          var a = $(this);
          var name = $(this).data('name');

          var info_createdby = $(this).data('createdby');
          var info_createdon = $(this).data('createdon');
          var info_updatedby = $(this).data('updatedby');
          var info_updatedon = $(this).data('updatedon');
          var htmlinfo = '';
          var editor = $("#modal-editor");

          if(info_createdby && info_createdon) {
            htmlinfo += 'Diinput oleh : <b>'+info_createdby+'</b> pada : <b>'+info_createdon+'</b><br />';
          }
          if(info_updatedby && info_updatedon) {
            htmlinfo += 'Diubah oleh : <b>'+info_updatedby+'</b> pada : <b>'+info_updatedon+'</b>';
          }

          $('[name=<?=COL_FILENAME?>]', editor).val(name);
          $('.label-info', editor).html(htmlinfo);

          editor.modal("show");
          $(".btn-ok", editor).unbind('click').click(function() {
            var dis = $(this);
            dis.html("Loading...").attr("disabled", true);
            $('#form-editor').ajaxSubmit({
                dataType: 'json',
                url : a.attr('href'),
                success : function(data){
                    if(data.error==0){
                        window.location.reload();
                    }else{
                        $(".error-message", editor).html(data.error);
                    }
                },
                complete: function(data) {
                  dis.html("SIMPAN").attr("disabled", false);
                }
            });
          });
          return false;
      });
    }
  });

  $('.modal-popup').click(function(){
      var a = $(this);
      var editor = $("#modal-editor");

      editor.modal("show");
      $(".btn-ok", editor).unbind('click').click(function() {
        var dis = $(this);
        dis.html("Loading...").attr("disabled", true);
        $('#form-editor').ajaxSubmit({
            dataType: 'json',
            url : a.attr('href'),
            success : function(data){
                if(data.error==0){
                    window.location.reload();
                }else{
                    $(".error-message", editor).html(data.error);
                }
            },
            complete: function(data) {
              dis.html("SIMPAN").attr("disabled", false);
            }
        });
      });
      return false;
  });
  $('#cekbox').click(function(){
      if($(this).is(':checked')){
          $('.cekbox').prop('checked',true);
          console.log('clicked');
      }else{
          $('.cekbox').prop('checked',false);
      }
  });
});
</script>
