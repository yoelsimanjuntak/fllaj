<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 30/01/2020
 * Time: 22:01
 */
$user = GetLoggedUser();
?>
<style>
div.dataTables_info {
    padding-top: 0 !important;
}
div.dataTables_length label {
    margin-bottom: 0 !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small class="text-sm font-weight-light"></small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-header">
              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
              </div>
            </div>
            <div class="card-body">
              <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered table-hover table-condensed">
                  <thead>
                    <tr>
                      <th>OPSI</th>
                      <th>NO. TIKET</th>
                      <th>WAKTU</th>
                      <th>STATUS</th>
                      <th>JUDUL</th>
                      <th>PELAPOR</th>
                      <th>KETERANGAN</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter" class="d-none">
  <div class="form-group row">
    <label class="col-sm-2 mb-0">PERIODE</label>
    <div class="col-sm-2">
      <input type="text" class="form-control datepicker text-right" name="filterDateFrom" value="<?=date('Y-m-1')?>" />
    </div>
    <label class="col-sm-1 mb-0 text-center">s.d</label>
    <div class="col-sm-2">
      <input type="text" class="form-control datepicker text-right" name="filterDateTo" value="<?=date('Y-m-d')?>" />
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 mb-0">STATUS</label>
    <div class="col-sm-5">
      <select class="form-control" name="filterIdOPD" style="width: 100%">
        <option value="">SEMUA</option>
        <option value="BARU">BARU</option>
        <option value="DIPROSES">DIPROSES</option>
        <option value="SELESAI">SELESAI</option>
      </select>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-status" role="dialog">
  <div class="modal-dialog modal-sm modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">UBAH STATUS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>STATUS</label>
          <select class="form-control" name="<?=COL_TCSTATUS?>" style="width: 100%">
            <option value="DIPROSES">DIPROSES</option>
            <option value="SELESAI">SELESAI</option>
          </select>
        </div>
        <div class="form-group mb-0">
          <label>KOMENTAR / CATATAN</label>
          <textarea class="form-control" name="<?=COL_TCCOMMENT?>" rows="3"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-block btn-success btn-submit">UPDATE</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-detail" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">

    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var modalStatus = $('#modal-status');
  var modalDetail = $('#modal-detail');

  modalStatus.on('hidden.bs.modal', function () {
    $('textarea',modalStatus).val('');
  });

  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/ticket/index-load')?>",
      "type": 'POST',
      "data": function(data) {
        var dateFrom = $('[name=filterDateFrom]', $('.divfilter')).val();
        var dateTo = $('[name=filterDateTo]', $('.divfilter')).val();
        var status = $('[name=filterStatus]', $('.divfilter')).val();

        data.dateFrom = dateFrom;
        data.dateTo = dateTo;
        data.status = status;
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "dom":"R<'row'<'col-sm-8 divfilter'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [[ 2, "desc" ]],
    "columnDefs": [
      {"targets":[0,1,3], "className":'nowrap text-center'},
      {"targets":[4,5,6], "className":'nowrap'},
      {"targets":[2], "className":'nowrap dt-body-right'}
    ],
    "columns": [
      {"orderable": false,"width": "10px"},
      {"orderable": true,"width": "50px"},
      {"orderable": true,"width": "10px"},
      {"orderable": false,"width": "10px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": false},
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-changestatus', $(row)).click(function() {
        var href = $(this).attr('href');
        modalStatus.modal('show');

        $('.btn-submit', modalStatus).unbind('click').click(function() {
          var stat = $('[name=TCStatus]', modalStatus).val();
          var comment = $('[name=TCComment]', modalStatus).val();

          $.post(href, {TCStatus: stat, TCComment: comment}, function(res) {
            res = JSON.parse(res);
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
              modalStatus.modal('hide');
            }

          }).done(function() {
            dt.DataTable().ajax.reload();
          })
          .fail(function() {
            toastr.error('SERVER ERROR');
          })
        });
        return false;
      });
      $('.btn-detail', $(row)).click(function() {
        var href = $(this).attr('href');
        $('.modal-content', modalDetail).load(href, function() {
          modalDetail.modal('show');
          return false;
        });
        return false;
      });
    }
  });
  $("div.divfilter").html($('#dom-filter').html());

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.divfilter")).change(function() {
    dt.DataTable().ajax.reload();
  });
});
</script>
