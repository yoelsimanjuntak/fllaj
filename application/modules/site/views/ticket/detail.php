<?php
if(empty($ticket)) {
  ?>
  <div class="modal-header">
    <h5 class="modal-title">TIKET TIDAK VALID</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
    </button>
  </div>
  <div class="modal-body">
    <p>Parameter tidak sesuai.</p>
  </div>
  <?php
} else {
  $htmlStatus = '';
  if($ticket[COL_TCSTATUS]=='BARU') $htmlStatus = '<span class="badge badge-secondary">'.$ticket[COL_TCSTATUS].'</span>';
  else if($ticket[COL_TCSTATUS]=='DIPROSES') $htmlStatus = '<span class="badge badge-info">'.$ticket[COL_TCSTATUS].'</span>';
  else if($ticket[COL_TCSTATUS]=='SELESAI') $htmlStatus = '<span class="badge badge-success">'.$ticket[COL_TCSTATUS].'</span>';
  ?>
  <div class="modal-header">
    <h5 class="modal-title">TIKET NO. <?=$ticket[COL_TCNO]?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
    </button>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-sm-12">
        <table class="table table-striped table-narrow elevation-1 text-sm">
          <tbody>
            <tr>
              <td style="width: 5px" class="nowrap text-muted">TANGGAL / WAKTU</td>
              <td style="width: 5px">:</td>
              <td class="font-weight-bold"><?=date('d-m-Y', strtotime($ticket[COL_CREATEDON]))?> / <?=date('H:i', strtotime($ticket[COL_CREATEDON]))?></td>
            </tr>
            <tr>
              <td style="width: 5px" class="nowrap text-muted">STATUS</td>
              <td style="width: 5px">:</td>
              <td class="font-weight-bold"><?=$htmlStatus?></td>
            </tr>
            <tr>
              <td style="width: 5px" class="nowrap text-muted">NAMA PELAPOR</td>
              <td style="width: 5px">:</td>
              <td class="font-weight-bold"><?=$ticket[COL_TCNAME]?></td>
            </tr>
            <tr>
              <td style="width: 5px" class="nowrap text-muted">JUDUL LAPORAN</td>
              <td style="width: 5px">:</td>
              <td class="font-weight-bold"><?=$ticket[COL_TCTITLE]?></td>
            </tr>
            <tr>
              <td colspan="3">
                <p><?=$ticket[COL_TCDESC]?></p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-sm-12">
        <h6 class="font-weight-bold">LAMPIRAN</h6>
        <div class="row">

          <?php
          $arrAttachment = $this->db
          ->where(COL_TCID, $ticket[COL_UNIQ])
          ->get(TBL_TICKET_ATTACHMENTS)
          ->result_array();

          foreach($arrAttachment as $att) {
            if(strpos(mime_content_type(MY_UPLOADPATH.$att[COL_TCFILENAME]), 'image') !== false) {
              ?>
              <div class="col-12 col-sm-4 col-md-4 d-flex align-items-stretch p-2">
                <div class="elevation-1" href="<?=MY_UPLOADURL.$att[COL_TCFILENAME]?>"
                data-toggle="lightbox"
                data-title="<?=$ticket[COL_TCTITLE]?>"
                data-gallery="gallery"
                style="background: url('<?=MY_UPLOADURL.$att[COL_TCFILENAME]?>');
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
                width: 100%;
                min-height: 100px;
                cursor: pointer;">
                </div>
              </div>
              <?php
            }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <?php
}
?>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<script type="text/javascript">
$(document).unbind('click').on('click', '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox();
});
</script>
