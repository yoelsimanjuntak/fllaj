<?php $data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_POSTID] . '" />',
        $d[COL_ISSUSPEND] ? '<small class="badge pull-left badge-danger">Suspend</small>' : (strtotime($d[COL_POSTEXPIREDDATE]) >= strtotime(date('Y-m-d')) ? '<small class="badge pull-left badge-success">Active</small>' : '<small class="badge pull-left badge-warning">Expired</small>'),
        date('Y-m-d', strtotime($d[COL_POSTDATE])),
        date('Y-m-d', strtotime($d[COL_POSTEXPIREDDATE])),
        anchor('site/post/edit/'.$d[COL_POSTID],$d[COL_POSTTITLE]),
        $d[COL_POSTCATEGORYNAME],
        '<a href="'.site_url('site/home/page/'.$d[COL_POSTSLUG]).'" target="_blank">'.site_url('post/view/'.$d[COL_POSTSLUG]).'</a>',

        //$d[COL_TOTALVIEW]
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?> <small class="font-weight-light">Data</small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12">
              <p>
                  <?=anchor('site/post/delete','<i class="far fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
                  <?php if($user[COL_ROLEID] == ROLEADMIN || $user[COL_ROLEID] == ROLEOPERATOR) { ?>
                      <?=anchor('site/post/activate','<i class="far fa-check"></i> Aktifkan',array('class'=>'cekboxaction btn btn-success btn-sm','confirm'=>'Apa anda yakin?'))?>
                      <?=anchor('site/post/activate/1','<i class="far fa-warning"></i> Suspend',array('class'=>'cekboxaction btn btn-warning btn-sm','confirm'=>'Apa anda yakin?'))?>
                  <?php } ?>
                  <?=anchor('site/post/add','<i class="far fa-plus"></i> Data Baru',array('class'=>'btn btn-primary btn-sm'))?>
              </p>
              <div class="card card-default">
                  <div class="card-body">
                      <form id="dataform" method="post" action="#">
                          <table id="datalist" class="table table-bordered table-hover">

                          </table>
                      </form>
                  </div>
              </div>
          </div>
      </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        var dataTable = $('#datalist').dataTable({
          "autoWidth":false,
          //"sDom": "Rlfrtip",
          "aaData": <?=$data?>,
          //"bJQueryUI": true,
          //"aaSorting" : [[5,'desc']],
          "scrollY" : '40vh',
          "scrollX": "120%",
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
          "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
          "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
          "order": [[ 2, "desc" ]],
          "aoColumns": [
              {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","width":"10px","bSortable":false},
              {"sTitle": "Status","bSortable":false},
              {"sTitle": "Tanggal"},
              {"sTitle": "Tanggal Exp."},
              {"sTitle": "Title"},
              {"sTitle": "Category"},
              {"sTitle": "URL"},
              //{"sTitle": "Total View", "width": "10%"}
          ]
        });
        $('#cekbox').click(function(){
            if($(this).is(':checked')){
                $('.cekbox').prop('checked',true);
                console.log('clicked');
            }else{
                $('.cekbox').prop('checked',false);
            }
        });
    });
</script>
