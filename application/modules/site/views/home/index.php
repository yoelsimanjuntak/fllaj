<style>
.todo-list>li:hover {
    background-color: #ccc;
}
.carousel-container {
  min-height: 300px;
}

.badge-misi {
  font-size: 11pt !important;
  font-weight: 400;
  position: absolute;
  left: 50% !important;
  top: -8px;
  margin: 0 !important;
}

#section-data .card-inner {
  transition: transform .2s;
}
#section-data .card-inner:hover {
  transform: scale(1.1);
  z-index: 1;
}
.timeline::before {
  display: none;
}
#section-data .content-header-bg::after {
  background: url(<?=MY_IMAGEURL.'footer-map-bg.png'?>) no-repeat scroll left top / 100% auto;
  content: "";
  height: 100%;
  left: 0;
  opacity: 0.1;
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 1;
}
#tbl-data-kec th {
  vertical-align: middle;
  text-align: center;
}
</style>
<?php
$carouselDir =  scandir(MY_IMAGEPATH.'slide/');
$carouselArr = array();
foreach (scandir(MY_IMAGEPATH.'slide/') as $file) {
  if(strpos(mime_content_type(MY_IMAGEPATH.'slide/'.$file), 'image') !== false) {;
    $carouselArr[MY_IMAGEURL.'slide/'.$file] = filemtime(MY_IMAGEPATH.'slide/'. $file); //$carouselArr[] = MY_IMAGEURL.'slide/'.$file;
  }
}
arsort($carouselArr);
$carouselArr = array_keys($carouselArr);
?>
<div class="content-wrapper">
  <div class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <!-- SLIDER -->
          <?php
          if(!empty($carouselArr)) {
            ?>
            <div id="carouselMain" class="carousel slide carousel-fade mb-3" data-ride="carousel" style="border: 4px solid #0F406E; border-radius: .25rem; box-shadow: 0 0 5px #00000020, 0 3px 5px rgba(0,0,0,.2);">
              <div class="carousel-inner">
                <?php
                for($i=0; $i<count($carouselArr); $i++) {
                  ?>
                  <div class="carousel-item <?=$i==0?'active':''?>">
                    <!--<img class="d-block" src="<?=$carouselArr[$i]?>" style="height: 200px">-->
                    <div class="d-block w-100" style="background: url('<?=$carouselArr[$i]?>'); background-size: cover; background-repeat: no-repeat; height: 25rem">
                    </div>
                  </div>
                  <?php
                }
                ?>
              </div>
              <a class="carousel-control-prev" href="#carouselMain" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselMain" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
              <ol class="carousel-indicators">
                <?php
                for($i=0; $i<count($carouselArr); $i++) {
                  ?>
                  <li data-target="#carouselMain" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                  <?php
                }
                ?>
              </ol>
              <!--<div class="d-block pt-2" style="background: #FFFF02 !important;">
                <marquee class="font-weight-bold text-dark">SELAMAT DATANG DI <?=$this->setting_web_name?> (<?=strtoupper($this->setting_web_desc)?>)</marquee>
              </div>-->
            </div>
            <?php
          }
          ?>
          <!-- SLIDER -->
        </div>
        <div class="col-sm-12">
          <div class="card">
            <div class="card-header bg-navy" style="background: #0F406E !important">
              <h5 class="card-title font-weight-bold m-0"><i class="far fa-newspaper"></i>&nbsp;&nbsp;BERITA TERKINI</h5>
            </div>
            <div id="berita" class="card-body pb-0" style="max-height: 20rem; overflow-y: scroll">
              <?php
              if(!empty($berita)) {
                $n=0;
                foreach($berita as $b) {
                  $n++;
                  $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
                  $img = $this->db->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
                  ?>
                  <div class="d-block pb-3 mb-3" <?=$n<count($berita)?'style="border-bottom: 1px solid #dedede"':''?>>
                    <div class="row">
                      <div class="col-sm-3">
                        <div class="img" style="
                        height: 150px;
                        width: 100%;
                        background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_FILENAME]:MY_IMAGEURL.'no-image.png'?>');
                        background-size: cover;
                        background-repeat: no-repeat;
                        background-position: center;
                        border: 4px solid #facd48;
                        ">
                        </div>
                      </div>
                      <div class="col-sm-9">
                        <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>"><h6 class="text-navy mb-0"><?=$b[COL_POSTTITLE]?></h6></a>
                        <p class="mb-0">
                          <small class="text-muted"><?=date('d-m-Y H:i', strtotime($b[COL_CREATEDON]))?></small>
                          <small class="text-muted float-right d-none">dilihat <strong><?=$b[COL_TOTALVIEW]?></strong> kali</small>
                        </p>
                        <p class="mb-2">
                          <?=strlen($strippedcontent) > 400 ? substr($strippedcontent, 0, 400) . "..." : $strippedcontent ?>
                        </p>
                      </div>
                    </div>
                  </div>
                  <?php
                }
                ?>
                <?php
              } else {
                echo '<p>Belum ada data.</p>';
              }
              ?>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <a href="<?=site_url('site/home/post/1')?>" class="btn bg-navy font-weight-bold" style="background: #0F406E !important"><i class="far fa-list"></i>&nbsp;LIHAT SEMUA</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<script type="text/javascript">
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox({
    alwaysShowClose: true
  });
});
var scrollHeight = document.getElementById('berita').clientHeight;
setTimeout('animateScroll()', 1000);
function animateScroll() {
  $('#berita').animate({scrollTop : 320},10000, function(){
    $(this).scrollTop(0);
    animateScroll();
  });
}
</script>
