<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div id="card-laporan" class="card card-outline card-primary">
          <div class="card-header">
            <div class="card-tools">
              <button type="button" class="btn btn-tool btn-refresh-laporan"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="data-laporan" method="post" action="#">
                <table id="dt-laporan" class="table table-bordered" style="white-space: nowrap;">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Waktu</th>
                      <th>Nama</th>
                      <th>NIK</th>
                      <th>No. HP</th>
                      <th>Alamat</th>
                      <th>Laporan</th>
                    </tr>
                  </thead>
                </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-popup" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Laporan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="far fa-sm fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body p-0">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var dtLaporan = $('#dt-laporan').dataTable({
    "autoWidth" : false,
    "scrollY" : '40vh',
    "processing": true,
    "serverSide": true,
    "ajax": {
      url : "<?=site_url('site/home/report-load')?>",
      type : 'POST'
    },
    "iDisplayLength": 25,
    "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Semua"]],
    "dom":"R<'row'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [],
    "columnDefs": [
      {"targets":[1], "className":'dt-body-right'},
      {"targets":[0], "className":'text-center'}
    ],
    "columns": [
      {"orderable": false},
      {"orderable": true},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-view-laporan', $(row)).click(function(){
        var a = $(this);
        var modalPopup = $("#modal-popup");

        $('.modal-body', modalPopup).load(a.attr('href'), function() {
          modalPopup.modal("show");
        });
        return false;
      });
    }
  });

  $('.btn-refresh-laporan', $('#card-laporan')).unbind().click(function() {
    $('#dt-laporan').DataTable().ajax.reload();
  });
});
</script>
