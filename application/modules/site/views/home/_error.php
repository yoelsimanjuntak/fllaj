<div class="content-wrapper">
  <section class="content mt-5">
    <div class="row">
      <div class="col-sm-12">
        <div class="error-page">
          <h2 class="headline text-yellow"> 404</h2>
          <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Halaman tidak ditemukan.</h3>
            <p>
                Silakan hubungi Administrator Sistem untuk info lebih lanjut.
                Sementara itu, anda dapat beralih <a href="<?=site_url()?>">KE HALAMAN UTAMA</a> untuk melanjutkan.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
