<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row mt-4">
        <div class="col-sm-12">
          <div class="text-center">
            <h3 class="m-0 text-navy pl-2 pr-2"><?=$title?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="card card-outline card-navy">
            <div class="card-header d-flex p-0">
              <ul class="nav nav-pills p-2">
                <li class="nav-item"><a class="nav-link active" href="#tabForm" data-toggle="tab"><i class="far fa-pencil-square"></i>&nbsp;&nbsp;Formulir Pengaduan</a></li>
                <li class="nav-item"><a class="nav-link" href="#tabLacak" data-toggle="tab"><i class="far fa-file-search"></i>&nbsp;&nbsp;Telusuri Pengaduan</a></li>
              </ul>
            </div>
            <div class="card-body">
              <div class="tab-content">
                <div class="tab-pane active" id="tabForm">
                  <form id="form-lapor" method="post" action="<?=current_url()?>" class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group mb-0">
                            <label>NAMA</label>
                            <input type="text" name="<?=COL_TCNAME?>" class="form-control" placeholder="Nama Lengkap" required />
                          </div>
                        </div>
                        <div class="col-sm-4 mb-0">
                          <label>EMAIL</label>
                          <input type="text" name="<?=COL_TCEMAIL?>" class="form-control" placeholder="Alamat Email" required />
                        </div>
                        <div class="col-sm-4 mb-0">
                          <label>NO. HP</label>
                          <input type="text"  name="<?=COL_TCPHONE?>" class="form-control" placeholder="No. HP (opsional)" />
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>JUDUL</label>
                      <input type="text"  name="<?=COL_TCTITLE?>" class="form-control" placeholder="Judul Aduan" required />
                    </div>
                    <div class="form-group">
                      <label>RINCIAN</label>
                      <textarea rows="5"  name="<?=COL_TCDESC?>" class="form-control" placeholder="Detil Aduan" required></textarea>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-6">
                        <label>LAMPIRAN (OPSIONAL, MAX. 5)</label>
                        <button type="button" id="btn-add-attachment" class="btn btn-xs btn-secondary pull-right"><i class="far fa-plus"></i>&nbsp;TAMBAH</button>
                        <div class="row">
                          <div id="div-attachment" class="col-sm-12">
                            <div class="input-group mb-2 d-none">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fad fa-image"></i></span>
                              </div>
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" name="file[]" disabled="disabled">
                                <label class="custom-file-label" for="userfile">PILIH FILE</label>
                              </div>
                              <div class="input-group-append">
                                <button type="button" class="btn btn-xs btn-danger pl-3 pr-3 btn-remove-attachment"><i class="far fa-times"></i></button>
                              </div>
                            </div>
                            <div class="input-group mb-2">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fad fa-image"></i></span>
                              </div>
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" name="file[]">
                                <label class="custom-file-label" for="userfile">PILIH FILE</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group text-center mb-0">
                      <button type="submit" class="btn btn-primary"><i class="far fa-file-check"></i>&nbsp;&nbsp;SUBMIT ADUAN</button>
                    </div>
                  </form>
                </div>
                <div class="tab-pane" id="tabLacak">
                  <div class="form-group mb-0">
                    <label>NO. TIKET PENGADUAN</label>
                    <div class="row">
                      <div class="col-sm-6">
                        <input type="text"  name="searchTicketNo" class="form-control" placeholder="No. Tiket" required />
                      </div>
                      <div class="col-sm-6">
                        <button type="button" id="btn-search-ticket" data-toggle="modal" data-target="#modal-detail" class="btn bg-navy" style="background: #0F406E !important"><i class="far fa-search"></i>&nbsp;CARI</button>
                      </div>
                    </div>
                    <p class="text-sm mt-2 mb-0 text-info">
                      <i class="far fa-info-circle"></i>&nbsp;Masukkan nomor tiket pengaduan yang sudah dikirim melalui email anda.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-detail">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">

    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  bsCustomFileInput.init();
  $('#btn-add-attachment').click(function() {
    var el = $('#div-attachment').find('.input-group.d-none').clone();
    el.find('input[type=file]').attr('disabled', false);
    el.removeClass('d-none').appendTo($('#div-attachment'));
    bsCustomFileInput.init();

    $('.btn-remove-attachment', $(el)).click(function() {
      $(this).closest('.input-group').remove();
    });
  });
  $('#form-lapor').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i> HARAP TUNGGU');
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 2000);
          }
        },
        error: function() {
          console.log('error cb');
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
  $('#btn-search-ticket').click(function() {
    var tcId = $('[name=searchTicketNo]').val();
    $('.modal-content', $('#modal-detail')).load('<?=site_url('site/home/search-ticket')?>', {TCId: tcId}, function() {
      $('#modal-detail').modal('show');
      return false;
    });
  });
});
</script>
