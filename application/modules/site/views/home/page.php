<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row mt-4">
        <div class="col-sm-12">
          <div class="text-center">
            <h3 class="m-0 text-navy pl-2 pr-2"><?=$data[COL_POSTTITLE]?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="card card-outline card-navy">
            <div class="card-header p-0">
              <?php
              $files = $this->db->where(COL_POSTID, $data[COL_POSTID])->get(TBL__POSTIMAGES)->result_array();
              if(!empty($files)) {
                if(count($files) > 1) {
                  ?>
                  <div class="row">
                    <?php
                    foreach($files as $f) {
                      if(strpos(mime_content_type(MY_UPLOADPATH.$f[COL_FILENAME]), 'image') !== false) {
                        ?>
                        <div class="col-12 col-sm-6 col-md-6 d-flex align-items-stretch p-2">
                          <div href="<?=MY_UPLOADURL.$f[COL_FILENAME]?>"
                          data-toggle="lightbox"
                          data-title="<?=$data[COL_POSTTITLE]?>"
                          data-gallery="gallery"
                          style="background: url('<?=MY_UPLOADURL.$f[COL_FILENAME]?>');
                                  background-size: cover;
                                  background-repeat: no-repeat;
                                  background-position: center;
                                  width: 100%;
                                  min-height: 300px;
                                  cursor: pointer;">
                          </div>
                        </div>
                        <?php
                      } else {
                        ?>
                        <div class="col-12 col-sm-12 col-md-12 mb-3 d-flex align-items-stretch">
                          <embed src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" width="100%" height="600" />
                        </div>
                        <?php
                      }
                      ?>
                    <?php
                    }
                    ?>
                  </div>
                  <?php
                } else {
                  ?>
                  <div class="row">
                    <div class="col-12 text-center">
                      <div href="<?=MY_UPLOADURL.$files[0][COL_FILENAME]?>"
                      data-toggle="lightbox"
                      data-title="<?=$data[COL_POSTTITLE]?>"
                      data-gallery="gallery"
                      style="width: 100%; min-height: 300px; cursor: pointer;">
                      <img src="<?=MY_UPLOADURL.$files[0][COL_FILENAME]?>" style="max-width: 100%" />
                      </div>
                    </div>
                  </div>
                  <?php
                }
              }
              ?>
            </div>
            <div class="card-body">
              <?=$data[COL_POSTCONTENT]?>
              <div class="row bt-1">
                <p class="font-sm text-muted">
                  <i class="fad fa-user"></i>&nbsp;&nbsp;<?=$data[COL_NAME]?><br  />
                  <i class="fad fa-calendar"></i>&nbsp;&nbsp;<?=date('d-m-Y H:i', strtotime($data[COL_CREATEDON]))?>
                </p>
              </div>
            </div>
            <div class="card-footer">
                <div id="disqus_thread"></div>
                <script>

                    /**
                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                    /*
                     */
                    var disqus_config = function () {
                        this.page.url = '<?=site_url('site/home/page/'.$data[COL_POSTSLUG])?>';  // Replace PAGE_URL with your page's canonical URL variable
                        this.page.identifier = '<?=$data[COL_POSTSLUG]?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                    };
                    (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = '<?=$this->setting_web_disqus_url?>';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
