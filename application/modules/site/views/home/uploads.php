<?php
$data = array();
$i = 0;
foreach ($res as $d) {
  $res[$i] = array(
      $d[COL_FILENAME],
      anchor(MY_UPLOADURL.$d[COL_FILEPATH], $d[COL_FILEPATH], array('target'=>'_blank')),
      $d[COL_CREATEDBY],
      date("Y-m-d H:i:s", strtotime($d[COL_CREATEDON]))
  );
  $i++;
}
$data = json_encode($res);
?>
<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row mt-4">
        <div class="col-sm-12">
          <div class="text-center">
            <h3 class="m-0 text-navy pl-2 pr-2"><?=$title?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="card card-outline card-navy">
            <div class="card-body">
              <form id="dataform" method="post" action="#">
                  <table id="datalist" class="table table-bordered table-hover display compact">

                  </table>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var dataTable = $('#datalist').dataTable({
    "autoWidth": false,
    //"sDom": "Rlfrtip",
    "aaData": <?=$data?>,
    //"bJQueryUI": true,
    //"aaSorting" : [[5,'desc']],
    "scrollY" : '44vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "order": [[ 0, "asc" ]],
    "columnDefs": [
        { className: "dt-body-right nowrap", "targets": [ 3 ] },
        { className: "nowrap", "targets": [ 0,1,2,3 ] }
    ],
    "aoColumns": [
        {"sTitle": "Dokumen"},
        {"sTitle": "URL","bSortable":false},
        {"sTitle": "Diinput Oleh"},
        {"sTitle": "Diinput Pada"}
    ]
  });
});
</script>
