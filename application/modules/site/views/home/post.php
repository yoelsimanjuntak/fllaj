<div class="content-wrapper">
  <div class="row mb-2 mt-4">
    <div class="col-sm-12 text-center">
      <h3 class="m-0 text-navy"><?=$title?></h3>
    </div>
  </div>
  <div class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <?php
          if(!empty($res)) {
            ?>
            <div class="row d-flex align-items-stretch">
              <?php
              foreach($res as $r) {
                $strippedcontent = strip_tags($r[COL_POSTCONTENT]);
                $img = $this->db->where(COL_POSTID, $r[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
                ?>
                <div class="col-12 col-sm-12 col-md-4 d-flex align-items-stretch">
                  <div class="card card-outline card-navy">
                    <div class="card-header">
                      <h3 class="card-title"><?=$r[COL_POSTTITLE]?></h3>
                    </div>
                    <div class="card-body p-0">
                      <div style="
                      width: 100%;
                      height: 25vh;
                      background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_FILENAME]:MY_IMAGEURL.'no-image.png'?>');
                      background-size: cover;
                      background-repeat: no-repeat;
                      background-position: center;
                      ">
                      </div>
                      <p class="p-3 mb-0"><?=strlen($strippedcontent) > 200 ? substr($strippedcontent, 0, 200) . "..." : $strippedcontent ?></p>
                    </div>
                    <div class="card-footer text-right">
                      <a href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>" class="text-navy">SELENGKAPNYA</a>
                    </div>
                  </div>
                </div>
                <?php
              }
              ?>
            </div>
            <div class="row d-flex pt-3 mb-3 align-items-stretch">
              <div class="col-sm-6 text-right">
                <?php
                if($start==0) {
                  ?>
                  <button type="button" class="btn btn-default" disabled>
                    <i class="far fa-chevron-left"></i>&nbsp;SEBELUMNYA
                  </button>
                  <?php
                } else {
                  ?>
                  <a href="?start=<?=$start-10?>" class="btn btn-default"><i class="far fa-chevron-left"></i>&nbsp;SEBELUMNYA</a>
                  <?php
                }
                ?>
              </div>
              <div class="col-sm-6 text-left">
                <?php
                if($start+10>$count) {
                  ?>
                  <button type="button" class="btn btn-default" disabled>
                    SELANJUTNYA&nbsp;<i class="far fa-chevron-right"></i>
                  </button>
                  <?php
                } else {
                  ?>
                  <a href="?start=<?=$start+10?>" class="btn btn-default">SELANJUTNYA&nbsp;<i class="far fa-chevron-right"></i></a>
                  <?php
                }
                ?>
              </div>
            </div>

            <?php
          } else {
            ?>
            <p class="text-center">Tidak ada data.</p>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
