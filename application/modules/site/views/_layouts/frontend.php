
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Font Awesome Icons -->
    <!--<link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/fontawesome-free/css/all.min.css">-->
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">

    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href="<?=MY_IMAGEURL.$this->setting_web_logo?>" />

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


    <!-- datatable reorder _ buttons ext + resp + print -->
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

    <!-- daterange picker -->
    <script src="<?=base_url()?>assets/js/moment.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.css">

    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

    <!-- my css -->
    <!--<link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">-->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/my.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/css/animate.css">

    <script>
    function startTime() {
      /*$.get('<?=site_url('site/ajax/now')?>', function(data) {
        var res = JSON.parse(data);
        $('#datetime').html(res.Day.toUpperCase()+', '+res.Date+' '+res.Month.toUpperCase()+' '+res.Year+' <span class="text-danger">'+res.Hour+':'+res.Minute+':'+res.Second+'</span>');
      });*/
      var today = new Date();
      $('#datetime').html(moment(today).format('DD')+' '+moment(today).format('MMMM').toUpperCase()+' '+moment(today).format('Y')+' <span style="color: #0000a4">'+moment(today).format('hh')+':'+moment(today).format('mm')+':'+moment(today).format('ss')+'</span>');
      var t = setTimeout(startTime, 1000);
    }
    $(document).ready(function() {
      startTime();
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      };
    });
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
    </script>
    <style>
    #footer-section::after {
      background-image: url('<?=MY_IMAGEURL.'bg-footer.png'?>');
      background-size: cover;
      background-position-y: bottom;
      content: "";
      height: 100%;
      left: 0;
      /*opacity: 0.1;*/
      position: absolute;
      top: 0;
      width: 100%;
      z-index: -1;
    }
    #footer-section table td {
      border-top: none !important;
    }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?=base_url()?>assets/preloader/images/<?=$this->setting_web_preloader?>) center no-repeat #fff;
    }
    .form-group .control-label {
        text-align: right;
        line-height: 2;
    }
    .head-middle-border:before {
      border-top: 2px solid #001f3f;
      content: "";
      height: 0;
      left: auto;
      position: absolute;
      right: 0;
      top: 50%;
      width: 100%;
    }
    .head-middle-border {
      position: relative;
    }
    .head-middle-border h1, .head-middle-border h2, .head-middle-border h3, .head-middle-border h4, .head-middle-border h5, .head-middle-border h6 {
      position: relative;
      background: #f4f6f9;
      display: inline-block;
    }
    .nowrap {
      white-space: nowrap;
    }
    .dt-body-right {
      text-align: right !important;
    }
    </style>
</head>
<body class="hold-transition layout-top-nav layout-navbar-fixed">
  <div class="se-pre-con"></div>
  <div class="wrapper">
    <nav class="main-header navbar pl-0 pr-0 navbar-dark" style="display: block; background: #0F406E">
      <div class="container-fluid">
        <div class="col-sm-12 pt-2 text-left">
          <span class="d-none d-sm-inline-block">
            <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image elevation" style="height: 60px; max-height: 60px">
            <img src="<?=MY_IMAGEURL.'logopupr.png'?>" alt="Logo" class="brand-image elevation" style="height: 60px; max-height: 60px">
            <img src="<?=MY_IMAGEURL.'logokemenhub.png'?>" alt="Logo" class="brand-image elevation" style="height: 60px; max-height: 60px">
          </span>
          <a href="<?=site_url()?>" class="navbar-brand pt-0" style="line-height: .75rem;">
            <h4 class="font-weight-bold mb-0" style="line-height: 1.25rem">
              <?=$this->setting_web_name?><br />
              <small class="font-weight-light d-none d-sm-inline-block"><?=$this->setting_web_desc?></small>
            </h4>
            <h6 class="mb-0">
              <?=$this->setting_org_name?><br />
              <?=GetSetting('SETTING_ORG_REGION')?>
            </h6>
          </a>
          <button type="button" class="navbar-toggle pull-right bg-transparent text-white" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="true" style="border: none">
            <span class="sr-only">Toggle navigation</span>
            <i class="fa fa-bars" aria-hidden="true"></i>
          </button>
        </div>
        <div class="navbar-collapse collapse pl-2" id="navbar-collapse">
          <ul class="navbar-nav">
            <li class="nav-item">
                <a href="<?=site_url()?>" class="nav-link">BERANDA</a>
            </li>
            <li class="nav-item dropdown">
              <a id="drpProfil" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="nav-link dropdown-toggle">PROFIL</a>
              <ul aria-labelledby="drpProfil" class="dropdown-menu border-0 shadow">
                <li><a href="<?=site_url('site/home/page/tentang-fllaj')?>" class="dropdown-item">Tentang FLLAJ</a></li>
                <li><a href="<?=site_url('site/home/page/visi-dan-misi-fllaj')?>" class="dropdown-item">Visi dan Misi FLLAJ</a></li>
              </ul>
            </li>
            <li class="nav-item d-sm-inline-block">
                <a href="<?=site_url('site/home/post/3')?>" class="nav-link">KEGIATAN</a>
            </li>
            <li class="nav-item dropdown">
              <a id="drpProfil" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="nav-link dropdown-toggle">WARTA</a>
              <ul aria-labelledby="drpProfil" class="dropdown-menu border-0 shadow">
                <li><a href="<?=site_url('site/home/post/1')?>" class="dropdown-item">Berita</a></li>
                <li><a href="<?=site_url('site/home/post/2')?>" class="dropdown-item">Artikel</a></li>
              </ul>
            </li>
            <li class="nav-item d-sm-inline-block">
                <a href="<?=site_url('site/home/lapor')?>" class="nav-link">PENGADUAN</a>
            </li>
            <li class="nav-item d-sm-inline-block">
                <a href="<?=site_url('site/home/uploads')?>" class="nav-link">PUSTAKA</a>
            </li>
            <li class="nav-item d-sm-inline-block">
                <a href="#" class="nav-link">HUBUNGI KAMI</a>
            </li>
            <li class="nav-item d-sm-inline-block">
                <a href="<?=site_url('site/user/login')?>" class="nav-link">CMS</a>
            </li>
          </ul>
        </div>
        <div class="d-none d-sm-inline-block pl-2 pr-2">
          <form class="form-inline">
            <ul class="navbar-nav mr-3" style="flex-direction: row">
              <li class="nav-item">
                <a href="<?=GetSetting('SETTING_ORG_FACEBOOK')?>" target="_blank" class="nav-link pr-1"><i class="fab fa-facebook-square"></i></a>
              </li>
              <li class="nav-item">
                <a href="<?=GetSetting('SETTING_ORG_TWITTER')?>" target="_blank" class="nav-link pl-1 pr-1"><i class="fab fa-twitter-square"></i></a>
              </li>
              <li class="nav-item">
                <a href="<?=GetSetting('SETTING_ORG_INSTAGRAM')?>" target="_blank" class="nav-link pl-1 pr-1"><i class="fab fa-instagram"></i></a>
              </li>
            </ul>
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </nav>
    <?=$content?>
    <div id="footer-section" class="bg-danger pt-3" style="position: relative; z-index:9; border-top: 1px solid #dee2e6;">
      <div class="content text-white" style="padding: 0 .5rem">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-6 p-2 pr-5">
              <span class="font-weight-bold" style="color: #facd48 !important">
                <?=nl2br($this->setting_web_name)?><br /><?=GetSetting('SETTING_ORG_NAME')?><br /><?=GetSetting('SETTING_ORG_REGION')?>
              </span>
              <p class="mt-2"><?=GetSetting('SETTING_WEB_WELCOMEMSG')?></p>
            </div>

            <div class="col-lg-6 p-2">
              <div class="row">
                <div class="col-lg-12 p-0">
                  <span class="font-weight-bold" style="color: #facd48 !important">
                    HUBUNGI KAMI
                  </span>
                </div>
                <div class="col-lg-12">
                  <div class="row">
                    <p>
                      <span class="font-weight-bold m-0" style="color: #facd48 !important">Alamat: </span><br />
                      <span><?=$this->setting_org_address?></span><br /><br />
                      <span class="font-weight-bold m-0" style="color: #facd48 !important">Telp / Fax :</span><br />
                      <span><?=$this->setting_org_phone?> / <?=$this->setting_org_fax?></span><br />
                      <span class="font-weight-bold m-0" style="color: #facd48 !important">Email :</span><br />
                      <span><?=$this->setting_org_mail?></span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
    <footer class="main-footer bg-danger" style="border-top: none !important; background: #0F406E !important">
        <div class="float-right d-none d-sm-inline">
          Version <b><?=$this->setting_web_version?></b>
        </div>
        <strong>Copyright &copy; <?=date("Y")?> <?=$this->setting_web_name?></strong>. Strongly developed by <b>Partopi Tao</b>.
    </footer>
  </div>
  <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.motio.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
      /*var element = document.querySelector('.content-wrapper');
      var panning = new Motio(element, {
        fps: 30,
        speedX: 30
      });
      panning.play();*/
  });
  </script>
</body>
</html>
