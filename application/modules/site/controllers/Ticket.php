<?php
class Ticket extends MY_Controller {
  public function __construct()
  {
    parent::__construct();
    if (!IsLogin()) {
      redirect('site/user/login');
    }
    if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEOPERATOR) {
      redirect('site/user/dashboard');
    }
  }

  public function index() {
    $data['title'] = "Rekapitulasi Pengaduan";
    $this->template->load('backend', 'site/ticket/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_TCNO,COL_CREATEDON,COL_TCTITLE,COL_TCNAME);
    $cols = array(COL_TCNO,COL_CREATEDON,COL_TCTITLE,COL_TCNAME,COL_TCDESC);

    $queryAll = $this->db->get(TBL_TICKET);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where('CAST('.TBL_TICKET.'.'.COL_CREATEDON.' as DATE) >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where('CAST('.TBL_TICKET.'.'.COL_CREATEDON.' as DATE) <= ', $dateTo);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->order_by(TBL_TICKET.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TICKET, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlStatus = '';
      if($r[COL_TCSTATUS]=='BARU') $htmlStatus = '<span class="badge badge-secondary">'.$r[COL_TCSTATUS].'</span>';
      else if($r[COL_TCSTATUS]=='DIPROSES') $htmlStatus = '<span class="badge badge-info">'.$r[COL_TCSTATUS].'</span>';
      else if($r[COL_TCSTATUS]=='SELESAI') $htmlStatus = '<span class="badge badge-success">'.$r[COL_TCSTATUS].'</span>';
      $htmlBtn = '<a href="'.site_url('site/ticket/detail/'.$r[COL_UNIQ]).'" class="btn btn-outline-info btn-xs btn-detail"><i class="far fa-info-circle"></i> DETAIL</a>';
      $htmlBtn .= '&nbsp;<a href="'.site_url('site/ticket/changestatus/'.$r[COL_UNIQ]).'" class="btn btn-outline-success btn-xs btn-changestatus" data-status="'.$r[COL_TCSTATUS].'"><i class="far fa-cog"></i> UPDATE</a>';
      $data[] = array(
        $htmlBtn,
        $r[COL_TCNO],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON])),
        $htmlStatus,
        $r[COL_TCTITLE],
        $r[COL_TCNAME],
        strlen($r[COL_TCDESC])>80?substr($r[COL_TCDESC],0,75).'...':$r[COL_TCDESC]
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function detail($id) {
    $data['ticket'] = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TICKET)
    ->row_array();
    $this->load->view('site/ticket/detail', $data, FALSE);
  }

  public function changestatus($id) {
    $ruser = GetLoggedUser();
    $rticket = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TICKET)
    ->row_array();
    if(empty($rticket)) {
      ShowJsonError('Parameter tidak valid.');
      return;
    }

    $this->db->trans_begin();
    try {
      $res = $this->db
      ->where(COL_UNIQ, $id)
      ->update(TBL_TICKET, array(COL_TCSTATUS=>$this->input->post(COL_TCSTATUS)));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }

      $res = $this->db->insert(TBL_TICKET_COMMENTS,
      array(COL_TCID=>$id,
      COL_TCCOMMENT=>$this->input->post(COL_TCCOMMENT),
      COL_CREATEDBY=>$ruser[COL_USERNAME],
      COL_CREATEDON=>date('Y-m-d H:i:s')));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }

      $res = $this->db->insert(TBL_TICKET_LOG,
      array(COL_TCID=>$id,
      COL_TCSTATUS=>$this->input->post(COL_TCSTATUS),
      COL_CREATEDBY=>$ruser[COL_USERNAME],
      COL_CREATEDON=>date('Y-m-d H:i:s')));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }

      $this->db->trans_commit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }

    ShowJsonSuccess('UPDATE BERHASIL');
    return;
  }
}
 ?>
