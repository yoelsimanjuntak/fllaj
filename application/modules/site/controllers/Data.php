<?php
class Data extends MY_Controller {
  public function __construct()
  {
      parent::__construct();
      if (!IsLogin()) {
          redirect('site/user/login');
      }
  }

  public function covid19() {

  }

  public function covid19_load() {
    $orderdef = array(COL_CREATE_DATE=>'desc');
    $orderables = array(null,COL_CREATE_DATE);
    $cols = array(COL_CREATE_DATE, COL_CREATE_BY);

    $i = 0;
    foreach($cols as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        }else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(isset($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(isset($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }
    $query = $this->db->get(TBL_COVID19_DATA);
    $data = [];

    foreach($query->result_array() as $r) {
      $data[] = array(
        '<input type="checkbox" class="cekbox checkbox-data" name="cekbox[]" value="' . $r[COL_UNIQ] . '" />',
        date('Y-m-d H:i:s', strtotime($r[COL_CREATE_DATE])),
        $r[COL_CREATE_BY],
        number_format($r[COL_JLH_OTG_PANTAU], 0),
        number_format($r[COL_JLH_OTG_SELESAI], 0),
        number_format($r[COL_JLH_ODP_PANTAU], 0),
        number_format($r[COL_JLH_ODP_SELESAI], 0),
        number_format($r[COL_JLH_PDP_PANTAU], 0),
        number_format($r[COL_JLH_PDP_SEHAT], 0),
        number_format($r[COL_JLH_SUSPECT_PASSED], 0),
        number_format($r[COL_JLH_SUSPECT_RECOVERED], 0)
      );
    }


    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $query->num_rows(),
      "recordsTotal" => $this->db->count_all_results(TBL_COVID19_DATA),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function covid19_load_kec() {
    $orderdef = array(COL_NM_KECAMATAN=>'asc');
    $orderables = array();
    $cols = array();

    $i = 0;
    foreach($cols as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        }else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(isset($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(isset($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }
    $q = @"
select * from (
  select
  kec.Kd_Kecamatan,
  kec.Nm_Kecamatan,
  ifnull((select dat.Jlh_OTGP from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_OTGP,
  ifnull((select dat.Jlh_OTGS from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_OTGS,
  ifnull((select dat.Jlh_ODP from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_ODP,
  ifnull((select dat.Jlh_ODPS from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_ODPS,
  ifnull((select dat.Jlh_PDP from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_PDP,
  ifnull((select dat.Jlh_Positif from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_Positif,

  ifnull((select dat.Jlh_Suspect from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_Suspect,
  ifnull((select dat.Jlh_Probable from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_Probable,
  ifnull((select dat.Jlh_Konfirmasi from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_Konfirmasi,
  ifnull((select dat.Jlh_KontakErat from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_KontakErat,
  ifnull((select dat.Jlh_PelakuPerjalanan from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_PelakuPerjalanan,
  ifnull((select dat.Jlh_Discarded from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_Discarded,
  ifnull((select dat.Jlh_SelesaiIsolasi from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_SelesaiIsolasi,
  ifnull((select dat.Jlh_Kematian from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_Kematian,
  (select dat.Create_Date from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1) as Create_Date,
  (select dat.Create_By from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1) as Create_By
  from covid19_kecamatan kec
) tbl order by tbl.Nm_Kecamatan asc
    ";
    $query = $this->db->query($q);
    $data = [];
    $n=0;
    foreach($query->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('site/data/covid19-add-kec/'.$r[COL_KD_KECAMATAN]).'" class="btn-add-data" data-kec="'.$r[COL_NM_KECAMATAN].'"><i class="fad fa-clipboard"></i></a>',
        $r[COL_NM_KECAMATAN],
        !empty($r[COL_CREATE_DATE])?date('Y-m-d H:i', strtotime($r[COL_CREATE_DATE])):'-',
        $r[COL_CREATE_BY],
        number_format($r[COL_JLH_SUSPECT], 0),
        number_format($r[COL_JLH_PROBABLE], 0),
        number_format($r[COL_JLH_KONFIRMASI], 0),
        number_format($r[COL_JLH_KONTAKERAT], 0),
        number_format($r[COL_JLH_PELAKUPERJALANAN], 0),
        number_format($r[COL_JLH_DISCARDED], 0),
        number_format($r[COL_JLH_SELESAIISOLASI], 0),
        number_format($r[COL_JLH_KEMATIAN], 0)
      );
      $n++;
    }


    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $query->num_rows(),
      "recordsTotal" => 0,
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function covid19_add() {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }

      $user = GetLoggedUser();
      if(!empty($_POST)){
          $data = array(
              COL_TANGGAL => date('Y-m-d'),
              COL_JLH_ODP_PANTAU => $this->input->post(COL_JLH_ODP_PANTAU),
              COL_JLH_ODP_SELESAI => $this->input->post(COL_JLH_ODP_SELESAI),
              COL_JLH_OTG_PANTAU => $this->input->post(COL_JLH_OTG_PANTAU),
              COL_JLH_OTG_SELESAI => $this->input->post(COL_JLH_OTG_SELESAI),
              COL_JLH_PDP_PANTAU => $this->input->post(COL_JLH_PDP_PANTAU),
              COL_JLH_PDP_SEHAT => $this->input->post(COL_JLH_PDP_SEHAT),
              COL_JLH_SUSPECT_PASSED => $this->input->post(COL_JLH_SUSPECT_PASSED),
              COL_JLH_SUSPECT_RECOVERED => $this->input->post(COL_JLH_SUSPECT_RECOVERED),

              COL_CREATE_BY=>$user[COL_USERNAME],
              COL_CREATE_DATE=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_COVID19_DATA, $data);
          if($res) {
            ShowJsonSuccess("Berhasil");
          } else {
            ShowJsonError("Gagal");
          }
      }
  }

  public function covid19_add_kec($id) {
      if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          redirect('user/dashboard');
      }

      $user = GetLoggedUser();
      if(!empty($_POST)){
          $data = array(
              COL_KD_KECAMATAN => $id,
              COL_JLH_OTGP => $this->input->post(COL_JLH_OTGP),
              COL_JLH_OTGS => $this->input->post(COL_JLH_OTGS),
              COL_JLH_ODP => $this->input->post(COL_JLH_ODP),
              COL_JLH_ODPS => $this->input->post(COL_JLH_ODPS),
              COL_JLH_PDP => $this->input->post(COL_JLH_PDP),
              COL_JLH_POSITIF => $this->input->post(COL_JLH_POSITIF),

              COL_JLH_SUSPECT => $this->input->post(COL_JLH_SUSPECT),
              COL_JLH_PROBABLE => $this->input->post(COL_JLH_PROBABLE),
              COL_JLH_KONFIRMASI => $this->input->post(COL_JLH_KONFIRMASI),
              COL_JLH_KONTAKERAT => $this->input->post(COL_JLH_KONTAKERAT),
              COL_JLH_PELAKUPERJALANAN => $this->input->post(COL_JLH_PELAKUPERJALANAN),
              COL_JLH_DISCARDED => $this->input->post(COL_JLH_DISCARDED),
              COL_JLH_SELESAIISOLASI => $this->input->post(COL_JLH_SELESAIISOLASI),
              COL_JLH_KEMATIAN => $this->input->post(COL_JLH_KEMATIAN),

              COL_CREATE_BY=>$user[COL_USERNAME],
              COL_CREATE_DATE=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_COVID19_KECAMATAN_DATA, $data);
          if($res) {
            ShowJsonSuccess("Berhasil");
          } else {
            $err = $this->db->error();
            ShowJsonError("Gagal. ".$err['message']);
          }
      }
  }

  public function covid19_delete() {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
        $this->db->delete(TBL_COVID19_DATA, array(COL_UNIQ => $datum));
        $deleted++;
    }
    if ($deleted) {
        ShowJsonSuccess($deleted." data dihapus");
    } else {
        ShowJsonError("Tidak ada dihapus");
    }
  }

  public function covid19_load_detail() {
    $orderdef = array(COL_TGL_INSERT=>'desc');
    $orderables = array(null,COL_NIK,COL_NAMA,COL_V_KECAMATAN,COL_V_DESA);
    $cols = array(COL_NIK,COL_NAMA,COL_V_KECAMATAN,COL_V_DESA);

    $i = 0;
    foreach($cols as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        }else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(isset($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(isset($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }
    $query = $this->db->get(TBL_COVID19_DATA_DETAIL);
    $data = [];

    foreach($query->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('site/data/covid19-data-detail/'.$r[COL_ID]).'" class="p-1 link-popup"><i class="fas fa-eye"></i></a>'.
        '<a href="'.site_url('site/data/covid19-data-forward/'.$r[COL_ID]).'" class="p-1 link-popup"><i class="fas fa-share"></i></a>'.
        '<a href="'.site_url('site/data/covid19-data-changestatus/'.$r[COL_ID]).'" class="p-1 link-popup"><i class="fas fa-user-edit"></i></a>',
        $r[COL_JENIS],
        $r[COL_NIK],
        $r[COL_NAMA],
        $r[COL_V_KECAMATAN],
        $r[COL_V_DESA],
        $r[COL_STATUS],
        $r[COL_STATUS_AKHIR],
        //date('Y-m-d H:i:s', strtotime($r[COL_TGL_INSERT])),
        //date('Y-m-d H:i:s', strtotime($r[COL_TGL_UPDATE]))
      );
    }


    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $query->num_rows(),
      "recordsTotal" => $this->db->count_all_results(TBL_COVID19_DATA_DETAIL),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function covid19_data_detail($id) {
    $rdata = $this->db->where(COL_ID, $id)->get(TBL_COVID19_DATA_DETAIL)->row_array();
    if(empty($rdata)) {
      echo 'Maaf, data tidak ditemukan';
      return;
    }

    $data['data'] = $rdata;
    $this->load->view('data/_view', $data);
  }

  public function covid19_data_forward($id) {
    $rdata = $this->db->where(COL_ID, $id)->get(TBL_COVID19_DATA_DETAIL)->row_array();
    if(empty($rdata)) {
      echo 'Maaf, data tidak ditemukan';
      return;
    }

    if(!empty($_POST)) {
      $rpuskesmas = $this->db
      ->where(COL_KD_PUSKESMAS, $this->input->post(COL_ID_PUSKESMAS))
      ->get(TBL_COVID19_PUSKESMAS)
      ->row_array();

      $kesehatan = $this->input->post(COL_KESEHATAN);
      $update = array();
      if(!empty($this->input->post(COL_STATUS))) $update[COL_STATUS] = $this->input->post(COL_STATUS);
      if(!empty($kesehatan)) $update[COL_KESEHATAN] = implode(',', $kesehatan);
      if(!empty($this->input->post(COL_DIAGNOSA))) $update[COL_DIAGNOSA] = $this->input->post(COL_DIAGNOSA);
      if(!empty($this->input->post(COL_SUHU_BADAN))) $update[COL_SUHU_BADAN] = $this->input->post(COL_SUHU_BADAN);
      if(!empty($this->input->post(COL_CATATAN_MEDIS))) $update[COL_CATATAN_MEDIS] = $this->input->post(COL_CATATAN_MEDIS);
      if(!empty($this->input->post(COL_ID_PUSKESMAS))) {
        $update[COL_ID_PUSKESMAS] = $this->input->post(COL_ID_PUSKESMAS);
        $update[COL_V_PUSKESMAS] = (!empty($rpuskesmas)?$rpuskesmas[COL_NM_PUSKESMAS]:null);
      }
      $res = true;
      if(!empty($update)) {
        $update[COL_TGL_UPDATE] = date('Y-m-d H:i:s');
        $res = $this->db->where(COL_ID, $id)->update(TBL_COVID19_DATA_DETAIL, $update);
      }
      if($res) {
        ShowJsonSuccess('BERHASIL');
      } else {
        ShowJsonError('SERVER ERROR');
      }
      return;
    } else {
      $data['data'] = $rdata;
      $this->load->view('data/_forward', $data);
    }
  }

  public function covid19_data_changestatus($id) {
    $rdata = $this->db->where(COL_ID, $id)->get(TBL_COVID19_DATA_DETAIL)->row_array();
    if(empty($rdata)) {
      echo 'Maaf, data tidak ditemukan';
      return;
    }

    $data['data'] = $rdata;
    $this->load->view('data/_view', $data);
  }
}
 ?>
