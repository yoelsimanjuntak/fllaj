<?php
class Home extends MY_Controller {

  public function index() {
    $data['title'] = 'Beranda';

    $this->load->model('mpost');
    $data['berita'] = $this->mpost->search(10,"",1);
		$this->template->load('frontend' , 'home/index', $data);
    //$this->load->view('home/index');
  }

  public function page($slug) {
    $data['title'] = 'Page';

    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where("(".TBL__POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL__POSTS.".".COL_POSTID." = '".$slug."')");
    $rpost = $this->db->get(TBL__POSTS)->row_array();
    if(!$rpost) {
        show_404();
        return false;
    }

    $this->db->where(COL_POSTID, $rpost[COL_POSTID]);
    $this->db->set(COL_TOTALVIEW, COL_TOTALVIEW."+1", FALSE);
    $this->db->update(TBL__POSTS);

    $data['title'] = $rpost[COL_POSTCATEGORYNAME];//strlen($rpost[COL_POSTTITLE]) > 50 ? substr($rpost[COL_POSTTITLE], 0, 50) . "..." : $rpost[COL_POSTTITLE];
    $data['data'] = $rpost;
    $this->template->load('frontend' , 'home/page', $data);
  }

  public function _404() {
    $data['title'] = 'Error';
    /*if(IsLogin()) {
      $this->template->load('backend' , 'home/_error', $data);
    } else {
      $this->template->load('frontend' , 'home/_error', $data);
    }*/
    $this->template->load('frontend' , 'home/_error', $data);
  }

  public function post($cat) {
    $rcat = $this->db
    ->where(COL_POSTCATEGORYID, $cat)
    ->get(TBL__POSTCATEGORIES)
    ->row_array();
    if(empty($rcat)) {
      show_error('Kategori tidak valid');
      return;
    }

    $data['title'] = $rcat[COL_POSTCATEGORYNAME];
    $start = !empty($_GET['start'])&&$_GET['start']>0?$_GET['start']:0;

    if(!empty($_GET['dateFrom'])) $this->db->where(TBL__POSTS.".".COL_CREATEDON." >= ", $_GET['dateFrom']);
    if(!empty($_GET['dateTo'])) $this->db->where(TBL__POSTS.".".COL_CREATEDON." <= ", $_GET['dateTo']);
    if(!empty($_GET['keyword'])) {
      $this->db->group_start();
      $this->db->like(TBL__POSTS.".".COL_POSTCONTENT, $_GET['keyword']);
      $this->db->or_like(TBL__POSTS.".".COL_POSTTITLE, $_GET['keyword']);
      $this->db->group_end();
    }

    $q = $this->db
    ->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner")
    ->where(TBL__POSTS.".".COL_POSTCATEGORYID, $cat)
    ->order_by(TBL__POSTS.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL__POSTS, TRUE);

    $data['res'] = $rpost = $this->db->query($q." LIMIT 10 OFFSET $start")->result_array();
    $data['count'] = $this->db->query($q)->num_rows();
    $data['start'] = $start;

    if(!empty($rpost)) {
      $data['title'] = $rpost[0][COL_POSTCATEGORYNAME];
    }

    $data['data'] = $rpost;
    $this->template->load('frontend' , 'home/post', $data);
  }

  public function report() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('site/user/dashboard');
    }

    $data['title'] = 'LAPORAN BENCANA';
    $this->template->load('backend' , 'home/report', $data);
  }

  public function report_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_CREATEDON);
    $cols = array(COL_NMPELAPOR, COL_NMNIK, COL_NMNOMORHP, COL_NMALAMAT, COL_NMLAPORAN);
    $queryTotal = $this->db->query("select * from tlaporan");

    $i = 0;
    foreach($cols as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        }else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(isset($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(isset($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->get_compiled_select(TBL_TLAPORAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];
    $n=0;
    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('site/home/report-view/'.$r[COL_UNIQ]).'" class="btn-view-laporan"><i class="fad fa-eye"></i></a>',
        !empty($r[COL_CREATEDON])?date('Y-m-d H:i', strtotime($r[COL_CREATEDON])):'-',
        $r[COL_NMPELAPOR],
        $r[COL_NMNIK],
        $r[COL_NMNOMORHP],
        $r[COL_NMALAMAT],
        strlen($r[COL_NMLAPORAN]) > 50 ? substr($r[COL_NMLAPORAN], 0, 50) . "..." : $r[COL_NMLAPORAN]
      );
      $n++;
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryTotal->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function report_view($id) {
    $rdata = $this->db
    ->select('tlaporan.*')
    ->where(COL_UNIQ, $id)
    ->get(TBL_TLAPORAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $html = @"
    <table class=\"table table-striped mb-0\" width=\"100%%\">
    <tr>
      <td>Waktu</td><td>:</td><td>%s</td>
    </tr>
    <tr>
      <td>Nama</td><td>:</td><td>%s</td>
    </tr>
    <tr>
      <td>NIK</td><td>:</td><td>%s</td>
    </tr>
    <tr>
      <td>Nomor HP</td><td>:</td><td>%s</td>
    </tr>
    <tr>
      <td>Alamat</td><td>:</td><td>%s</td>
    </tr>
    </table>
    <p class=\"pl-3 pr-3 pt-2\" style=\"white-space: pre-line; border-top: 1px solid #dedede\">%s</p>
    ";
    echo sprintf($html, (!empty($rdata[COL_CREATEDON])?date('Y-m-d H:i',strtotime($rdata[COL_CREATEDON])):'-'),$rdata[COL_NMPELAPOR],$rdata[COL_NMNIK],$rdata[COL_NMNOMORHP],$rdata[COL_NMALAMAT],$rdata[COL_NMLAPORAN]);
  }

  public function lapor() {
    $data['title'] = 'PORTAL PENGADUAN';
    if(!empty($_POST)) {
      $dat = array(
        COL_TCTITLE=>$this->input->post(COL_TCTITLE),
        COL_TCDESC=>$this->input->post(COL_TCDESC),
        COL_TCNAME=>$this->input->post(COL_TCNAME),
        COL_TCEMAIL=>$this->input->post(COL_TCEMAIL),
        COL_TCGENDER=>$this->input->post(COL_TCGENDER),
        COL_TCPHONE=>$this->input->post(COL_TCPHONE),
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      try {
        $config['upload_path'] = MY_UPLOADPATH;
        $config['allowed_types'] = "jpg|jpeg|png";
        $config['max_size']	= 512000;
        $config['max_width']  = 4000;
        $config['max_height']  = 4000;
        $config['overwrite'] = FALSE;
        $this->load->library('upload',$config);
        $arrAttachment = array();
        $datAttachment = array();
        $files = $_FILES;
        $cpt = count($_FILES['file']['name']);
        for($i=0; $i<$cpt; $i++)
        {
          if(!empty($files['file']['name'][$i])) {
            $_FILES['file']['name']= $files['file']['name'][$i];
            $_FILES['file']['type']= $files['file']['type'][$i];
            $_FILES['file']['tmp_name']= $files['file']['tmp_name'][$i];
            $_FILES['file']['error']= $files['file']['error'][$i];
            $_FILES['file']['size']= $files['file']['size'][$i];

            $this->upload->do_upload('file');
            $upl = $arrAttachment[] = $this->upload->data();
          }
        }
      } catch(Exception $ex) {
        ShowJsonError('Gagal mengupload lampiran. Silakan cek kembali lampiran anda. Pesan: '.$ex->getMessage());
        return;
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TICKET, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $ticketId = $this->db->insert_id();
        $res = $this->db
        ->where(COL_UNIQ, $ticketId)
        ->update(TBL_TICKET, array(COL_TCNO=>str_pad($ticketId, 5, "0", STR_PAD_LEFT)));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        foreach($arrAttachment as $attc) {
          $datAttachment[] = array(
            COL_TCID=>$ticketId,
            COL_TCFILENAME=>$attc['file_name']
          );
        }

        if(!empty($datAttachment)) {
          $res = $this->db->insert_batch(TBL_TICKET_ATTACHMENTS, $datAttachment);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        }

        $this->db->trans_commit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }

      ShowJsonSuccess('Aduan dengan NOMOR TIKET '.str_pad($ticketId, 5, "0", STR_PAD_LEFT).' berhasil dikirim. Silakan cek email untuk melihat NOMOR TIKET anda.');
      return;
    } else {
      $this->template->load('frontend' , 'home/lapor', $data);
    }

  }

  public function search_ticket() {
    $id = $this->input->post(COL_TCID);
    $data['ticket'] = $this->db
    ->where(COL_TCNO, $id)
    ->get(TBL_TICKET)
    ->row_array();
    $this->load->view('site/ticket/search', $data, FALSE);
  }

  public function uploads() {
    $data['title'] = 'Pustaka';
    $this->db->select('*,uc.Name as Nm_CreatedBy, uu.Name as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL__UPLOADS.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL__UPLOADS.".".COL_UPDATEDBY,"left");
    $this->db->order_by(COL_CREATEDON, 'desc');
    $data['res'] = $this->db->get(TBL__UPLOADS)->result_array();
    $this->template->load('frontend' , 'home/uploads', $data);
  }
}
 ?>
