<?php
class Upload extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEOPERATOR) {
      redirect('admin/dashboard');
    }
  }

  public function index() {
    $data['title'] = "Pustaka";
    $this->db->select('*,uc.Name as Nm_CreatedBy, uu.Name as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL__UPLOADS.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL__UPLOADS.".".COL_UPDATEDBY,"left");
    $this->db->order_by(COL_CREATEDON, 'desc');
    $data['res'] = $this->db->get(TBL__UPLOADS)->result_array();
    $this->template->load('backend', 'site/upload/index', $data);
  }

  public function add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['overwrite'] = FALSE;
      $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|ppt|pptx";
      $this->load->library('upload',$config);

      $data = array(
        COL_FILENAME => $this->input->post(COL_FILENAME),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        if($this->upload->do_upload('userfile')){
          $uploadData = $this->upload->data();
          $data[COL_FILEPATH] = $uploadData['file_name'];
        } else {
          throw new Exception('Error: '.$this->upload->display_errors());
        }

        $res = $this->db->insert(TBL__UPLOADS, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['overwrite'] = FALSE;
      $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|ppt|pptx";
      $this->load->library('upload',$config);

      $data = array(
        COL_FILENAME => $this->input->post(COL_FILENAME),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        if (!empty($_FILES['userfile']['name'])) {
          if($this->upload->do_upload('userfile')){
            $uploadData = $this->upload->data();
            $data[COL_FILEPATH] = $uploadData['file_name'];
          } else {
            throw new Exception('Error: '.$this->upload->display_errors());
          }
        }

        $res = $this->db->where(COL_UNIQ, $id)->update(TBL__UPLOADS, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function delete() {
    $ruser = GetLoggedUser();
    $data = $this->input->post('cekbox');
    $deleted = 0;
    $this->db->trans_begin();
    try {
      foreach ($data as $datum) {
        $rdata = $this->db->where(COL_UNIQ, $datum)->get(TBL__UPLOADS)->row_array();
        $res = $this->db
        ->where(COL_UNIQ, $datum)
        ->delete(TBL__UPLOADS);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        if(!empty($rdata)) {
          if(file_exists(MY_UPLOADPATH.$rdata[COL_FILEPATH])) {
            unlink(MY_UPLOADPATH.$rdata[COL_FILEPATH]);
          }
        }

      }
      $this->db->trans_commit();
      ShowJsonSuccess('OK');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada dihapus");
    }
  }
}
