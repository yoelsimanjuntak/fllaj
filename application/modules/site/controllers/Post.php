<?php
class Post extends MY_Controller {
  function __construct() {
    parent::__construct();
    /*if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        redirect('user/dashboard');
    }*/
    $this->load->model('mpost');
    ini_set('upload_max_filesize', '50M');
    ini_set('post_max_size', '50M');
  }

  function index() {
      if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEOPERATOR)) {
          redirect('user/dashboard');
      }
      $data['title'] = "Posts";
      $data['res'] = $this->mpost->getall();
      $this->template->load('backend' , 'post/index', $data);
  }

  function add() {
      if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEOPERATOR)) {
          redirect('site/user/dashboard');
      }
      $user = GetLoggedUser();
      $data['title'] = "Post";
      $data['edit'] = FALSE;

      if(!empty($_POST)){
          $data['data'] = $_POST;
          $rules = $this->mpost->rules();
          $this->form_validation->set_rules($rules);
          if($this->form_validation->run()){
              $id = GetLastID(TBL__POSTS, COL_POSTID) + 1;

              $config['upload_path'] = MY_UPLOADPATH;
              $config['allowed_types'] = "gif|jpg|jpeg|png|pdf";
              $config['max_size']	= 512000;
              $config['max_width']  = 4000;
              $config['max_height']  = 4000;
              $config['overwrite'] = FALSE;

              $this->load->library('upload',$config);
              $filesCount = count($_FILES['userfile']['name']);
              for($i = 0; $i < $filesCount; $i++) {
                  if(!empty($_FILES['userfile']['name'][$i])) {
                      $_FILES['file']['name'] = $_FILES['userfile']['name'][$i];
                      $_FILES['file']['type'] = $_FILES['userfile']['type'][$i];
                      $_FILES['file']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
                      $_FILES['file']['error'] = $_FILES['userfile']['error'][$i];
                      $_FILES['file']['size'] = $_FILES['userfile']['size'][$i];

                      // Upload file to server
                      if($this->upload->do_upload('file')){
                          // Uploaded file data
                          $fileData = $this->upload->data();
                          $uploadData[$i][COL_FILENAME] = $fileData['file_name'];
                      }
                      else {
                          $data['upload_errors'] = $this->upload->display_errors();
                          $this->template->load('backend' , 'post/form', $data);
                          return;
                      }
                  }
              }

              $data = array(
                  COL_POSTID => $id,
                  COL_POSTCATEGORYID => $this->input->post(COL_POSTCATEGORYID),
                  COL_POSTDATE => date('Y-m-d'),
                  COL_POSTTITLE => $this->input->post(COL_POSTTITLE),
                  COL_POSTSLUG => slugify($this->input->post(COL_POSTTITLE)),
                  COL_POSTCONTENT => $this->input->post(COL_POSTCONTENT),
                  COL_POSTEXPIREDDATE => date('Y-m-d', strtotime($this->input->post(COL_POSTEXPIREDDATE))),
                  COL_ISSUSPEND => ($user[COL_ROLEID] == ROLEADMIN ? ($this->input->post(COL_ISSUSPEND) ? $this->input->post(COL_ISSUSPEND) : false) : true),
                  COL_CREATEDBY => $user[COL_USERNAME],
                  COL_CREATEDON => date('Y-m-d H:i:s'),
                  COL_UPDATEDBY => $user[COL_USERNAME],
                  COL_UPDATEDON => date('Y-m-d H:i:s')
              );
              $res = $this->db->insert(TBL__POSTS, $data);
              if($res) {
                  if(!empty($uploadData)){
                      $id = $this->db->insert_id();
                      for($i = 0; $i < count($uploadData); $i++) {
                          $uploadData[$i][COL_POSTID] = $id;
                      }

                      $this->db->insert_batch(TBL__POSTIMAGES, $uploadData);
                  }
                  redirect('site/post/index');
              } else {
                  redirect(current_url()."?error=1");
              }
          }
          else {
              $this->template->load('backend' , 'post/form', $data);
          }
      }
      else {
          $this->template->load('backend' , 'post/form', $data);
      }
  }

  function edit($id) {
      if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEOPERATOR)) {
          redirect('user/dashboard');
      }
      $user = GetLoggedUser();
      $data['title'] = "Post";
      $data['edit'] = TRUE;
      $data['data'] = $edited = $this->db->where(COL_POSTID, $id)->get(TBL__POSTS)->row_array();
      if(empty($edited)){
          show_404();
          return;
      }

      if(!empty($_POST)){
          $data['data'] = $_POST;
          $rules = $this->mpost->rules(false);
          $this->form_validation->set_rules($rules);
          if($this->form_validation->run()){
              $config['upload_path'] = MY_UPLOADPATH;
              $config['allowed_types'] = "gif|jpg|jpeg|png|pdf";
              $config['max_size']	= 512000;
              $config['max_width']  = 4000;
              $config['max_height']  = 4000;
              $config['overwrite'] = FALSE;

              $this->load->library('upload',$config);
              $filesCount = count($_FILES['userfile']['name']);
              $files = $this->db->where(COL_POSTID, $id)->get(TBL__POSTIMAGES)->result_array();
              if($filesCount > 0 && !empty($_FILES['userfile']['name'][0])) {
                  for($i = 0; $i < $filesCount; $i++) {
                      $_FILES['file']['name'] = $_FILES['userfile']['name'][$i];
                      $_FILES['file']['type'] = $_FILES['userfile']['type'][$i];
                      $_FILES['file']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
                      $_FILES['file']['error'] = $_FILES['userfile']['error'][$i];
                      $_FILES['file']['size'] = $_FILES['userfile']['size'][$i];

                      // Upload file to server
                      if($this->upload->do_upload('file')){
                          // Uploaded file data
                          $fileData = $this->upload->data();
                          $uploadData[$i][COL_FILENAME] = $fileData['file_name'];
                          $uploadData[$i][COL_POSTID] = $id;
                      }
                      else {
                          $data['upload_errors'] = $this->upload->display_errors();
                          $this->template->load('backend' , 'post/form', $data);
                          return;
                      }
                  }

                  foreach($files as $f) {
                      unlink(MY_UPLOADPATH.$f[COL_FILENAME]);
                  }
              }

              $data = array(
                  COL_POSTCATEGORYID => $this->input->post(COL_POSTCATEGORYID),
                  COL_POSTDATE => date('Y-m-d'),
                  COL_POSTTITLE => $this->input->post(COL_POSTTITLE),
                  //COL_POSTSLUG => $this->input->post(COL_POSTSLUG) ? $this->input->post(COL_POSTSLUG) : str_replace(" ", "-", strtolower($this->input->post(COL_POSTTITLE))),
                  COL_POSTSLUG => slugify($this->input->post(COL_POSTTITLE)),
                  COL_POSTCONTENT => $this->input->post(COL_POSTCONTENT),
                  COL_POSTEXPIREDDATE => date('Y-m-d', strtotime($this->input->post(COL_POSTEXPIREDDATE))),
                  COL_ISSUSPEND => ($user[COL_ROLEID] == ROLEADMIN ? ($this->input->post(COL_ISSUSPEND) ? $this->input->post(COL_ISSUSPEND) : false) : true),
                  COL_UPDATEDBY => $user[COL_USERNAME],
                  COL_UPDATEDON => date('Y-m-d H:i:s')
              );

              $reg = $this->db->where(COL_POSTID, $id)->update(TBL__POSTS, $data);
              if($reg) {
                  if(!empty($uploadData)){
                      $this->db->insert_batch(TBL__POSTIMAGES, $uploadData);
                      foreach($files as $f) {
                          $this->db->delete(TBL__POSTIMAGES, array(COL_POSTIMAGEID => $f[COL_POSTIMAGEID]));
                      }
                  }

                  redirect(site_url('site/post/index'));
              }
              else redirect(current_url().'?error=1');
          }
          else {
              $this->template->load('backend' , 'post/form', $data);
          }
      }
      else {
          $this->template->load('backend' , 'post/form', $data);
      }
  }

  function delete(){
      if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEOPERATOR)) {
          redirect('site/user/dashboard');
      }
      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
          $this->db->delete(TBL__POSTS, array(COL_POSTID => $datum));
          $deleted++;

          $files = $this->db->where(COL_POSTID, $datum)->get(TBL__POSTIMAGES)->result_array();
          foreach($files as $f) {
              unlink(MY_UPLOADPATH.$f[COL_FILENAME]);
              $this->db->delete(TBL__POSTIMAGES, array(COL_POSTIMAGEID => $f[COL_POSTIMAGEID]));
          }
      }
      if($deleted){
          ShowJsonSuccess($deleted." data dihapus");
      }else{
          ShowJsonError("Tidak ada dihapus");
      }
  }

  function activate($suspend=false) {
      if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEOPERATOR)) {
          redirect('site/user/dashboard');
      }

      if(!IsLogin()) {
          ShowJsonError('Silahkan login terlebih dahulu');
          return;
      }
      $loginuser = GetLoggedUser();
      if(!$loginuser || ($loginuser[COL_ROLEID] != ROLEADMIN && $loginuser[COL_ROLEID] != ROLEOPERATOR)) {
          ShowJsonError('Anda tidak memiliki akses terhadap modul ini.');
          return;
      }
      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
          if($this->db->where(COL_POSTID, $datum)->update(TBL__POSTS, array(COL_ISSUSPEND=>$suspend))) {
              $deleted++;
          }
      }
      if($deleted){
          ShowJsonSuccess($deleted." data diubah");
      }else{
          ShowJsonError("Tidak ada data yang diubah");
      }
  }
}
 ?>
