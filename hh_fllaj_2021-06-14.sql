# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: hh_fllaj
# Generation Time: 2021-06-13 17:22:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`)
VALUES
	(1,'Berita','#f56954'),
	(2,'Artikel','#00a65a'),
	(3,'Kegiatan','#f39c12'),
	(5,'Lainnya','#3c8dbc');

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_postimages` WRITE;
/*!40000 ALTER TABLE `_postimages` DISABLE KEYS */;

INSERT INTO `_postimages` (`PostImageID`, `PostID`, `FileName`, `Description`)
VALUES
	(1,3,'photo-1506744038136-46273834b3fb.jpeg',NULL);

/*!40000 ALTER TABLE `_postimages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_posts` WRITE;
/*!40000 ALTER TABLE `_posts` DISABLE KEYS */;

INSERT INTO `_posts` (`PostID`, `PostCategoryID`, `PostDate`, `PostTitle`, `PostSlug`, `PostContent`, `PostExpiredDate`, `TotalView`, `LastViewDate`, `IsSuspend`, `FileName`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,5,'2021-06-13','Tentang FLLAJ','tentang-fllaj','<p style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<hr />\r\n<p>&nbsp;</p>\r\n','2050-12-31',5,NULL,0,NULL,'admin','2021-06-13 23:27:45','admin','2021-06-13 23:27:45'),
	(2,5,'2021-06-13','Visi dan Misi FLLAJ','visi-dan-misi-fllaj','<blockquote>\r\n<p style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n</blockquote>\r\n','2050-12-31',5,NULL,0,NULL,'admin','2021-06-13 23:29:23','admin','2021-06-13 23:29:23'),
	(3,1,'2021-06-13','Lorem Ipsum','lorem-ipsum','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse pulvinar luctus nisi, id pellentesque metus semper eget. Nulla leo sapien, euismod eget purus iaculis, efficitur pellentesque neque. In hac habitasse platea dictumst. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam sapien dolor, auctor quis viverra in, feugiat ut lectus. Fusce ipsum velit, consectetur at dictum sit amet, ultrices eu felis. Maecenas at nunc lacus. Phasellus placerat facilisis congue.</p>\r\n\r\n<p>Ut semper molestie ligula, eget interdum est lacinia eget. Mauris congue aliquam nibh, id condimentum felis tincidunt sodales. Morbi lacinia nec neque eget finibus. Integer a pharetra dolor. Aliquam vehicula ut dui a fringilla. Suspendisse et pellentesque mauris. Etiam viverra est sit amet erat condimentum, ut sodales nulla finibus. Donec feugiat odio et nulla vulputate blandit. Proin vel dapibus metus.</p>\r\n\r\n<p>Phasellus tincidunt neque urna, vitae semper ligula varius et. Phasellus nec neque non velit blandit fringilla. Vivamus sed justo enim. Vivamus imperdiet elit eu fermentum condimentum. Cras facilisis justo vitae diam ornare, convallis malesuada odio molestie. Duis sollicitudin, velit vel faucibus rutrum, lectus dui dictum orci, at posuere nisi risus sit amet dolor. Nam tincidunt, ligula in hendrerit commodo, magna enim gravida metus, eget accumsan nunc nunc non nulla. Proin blandit eros sit amet nibh faucibus, at viverra leo fringilla. Proin sodales turpis ex, eu volutpat velit eleifend et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>\r\n\r\n<p>Maecenas non tristique lacus. Donec viverra arcu vitae odio molestie, vel gravida turpis finibus. Mauris tempus varius porttitor. Phasellus non euismod urna. Sed hendrerit mattis arcu, id cursus risus porta eu. In varius, est ac ultrices feugiat, ipsum lectus ullamcorper enim, vitae facilisis erat metus in elit. Integer hendrerit ornare massa ut tincidunt. Nulla mollis mauris non porta ultricies. Aliquam erat volutpat. Vestibulum ac eros nulla.</p>\r\n\r\n<p>Duis non erat dui. Nulla luctus tincidunt diam consequat dapibus. Vivamus ut arcu non ex tempus tempor vitae sed turpis. Maecenas pulvinar magna lacus, vel tincidunt augue cursus eget. Quisque nec tincidunt urna, vitae fermentum tortor. Aenean ultrices justo lacus, at elementum purus tempus vitae. Pellentesque molestie blandit eleifend. Fusce rutrum commodo est quis faucibus. Suspendisse eu cursus lacus, id tincidunt tortor. Vivamus non odio mattis risus consectetur imperdiet. Sed ac tellus eget nisi tincidunt vehicula. Cras id volutpat leo. Cras fermentum, justo eget feugiat hendrerit, ante dolor suscipit purus, eget convallis nibh sapien vel diam. Vestibulum at tempus quam.</p>\r\n','2022-12-31',1,NULL,0,NULL,'admin','2021-06-13 23:31:38','admin','2021-06-13 23:31:38');

/*!40000 ALTER TABLE `_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Operator'),
	(3,'Guest');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','FLLAJ'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Forum Lalu Lintas dan Angkutan Jalan'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','DINAS PERHUBUNGAN'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Jl. Mayor Saur H. Purba\r\nKompleks Perkantoran Bukit Inspirasi Dolok Sanggul'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','dishub@humbanghasundutankab.go.id'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','-'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','bar.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0'),
	(16,'SETTING_ORG_REGION','SETTING_ORG_REGION','KABUPATEN HUMBANG HASUNDUTAN'),
	(17,'SETTING_WEB_WELCOMEMSG','SETTING_WEB_WELCOMEMSG','Wahana untuk menyinergikan tugas pokok dan fungsi setiap penyelenggaraan lalu lintas dan angkutan jalan dalam penyelenggaraan lalu lintas dan angkutan jalan.'),
	(18,'','SETTING_ORG_NAME','DINAS PERHUBUNGAN'),
	(19,'','SETTING_ORG_ADDRESS','Jl. Mayor Saur H. Purba\r\nKompleks Perkantoran Bukit Inspirasi Dolok Sanggul'),
	(20,'','SETTING_ORG_PHONE','-'),
	(21,'','SETTING_ORG_FAX','-'),
	(22,'','SETTING_ORG_MAIL','dishub@humbanghasundutankab.go.id'),
	(23,'','SETTING_ORG_FACEBOOK','https://web.facebook.com/PemkabHumbahas1'),
	(24,'','SETTING_ORG_TWITTER','https://twitter.com/pemkabhumbahas'),
	(25,'','SETTING_ORG_INSTAGRAM','https://www.instagram.com/diskominfohumbahas/');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _uploads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_uploads`;

CREATE TABLE `_uploads` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `FileType` enum('IMG','VID','DOC','OTHERS') DEFAULT NULL,
  `FileName` varchar(200) NOT NULL DEFAULT '',
  `FileDesc` text,
  `FilePath` varchar(200) NOT NULL DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_uploads` WRITE;
/*!40000 ALTER TABLE `_uploads` DISABLE KEYS */;

INSERT INTO `_uploads` (`Uniq`, `FileType`, `FileName`, `FileDesc`, `FilePath`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(2,NULL,'SK FLLAJ 2021',NULL,'Scan_SK_FLLAJ_2021.pdf','admin','2021-06-14 00:11:27',NULL,NULL),
	(3,NULL,'SOP FLLAJ 2021',NULL,'Scan_SOP_FLLAJ_2021.pdf','admin','2021-06-14 00:11:40',NULL,NULL),
	(4,NULL,'Program Kerja FLLAJ 2021',NULL,'Scan_Program_Kerja_FLLAJ_2021.pdf','admin','2021-06-14 00:11:50',NULL,NULL);

/*!40000 ALTER TABLE `_uploads` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(10) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(10) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(10) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`UserName`, `Email`, `CompanyID`, `Name`, `IdentityNo`, `BirthDate`, `ReligionID`, `Gender`, `Address`, `PhoneNumber`, `EducationID`, `UniversityName`, `FacultyName`, `MajorName`, `IsGraduated`, `GraduatedDate`, `YearOfExperience`, `RecentPosition`, `RecentSalary`, `ExpectedSalary`, `CVFilename`, `ImageFilename`, `RegisteredDate`)
VALUES
	('admin','yoelrolas@gmail.com',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17'),
	('operator','operator@fllaj.humbanghasundutankab.go.id',NULL,'Operator CMS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-06-06');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2021-06-13 23:23:38','::1'),
	('operator','8a1b65af29472146597995e631c33720',2,0,'2021-06-14 00:20:48','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ticket
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ticket`;

CREATE TABLE `ticket` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `TCNo` varchar(50) DEFAULT '',
  `TCStatus` enum('BARU','DIPROSES','SELESAI') NOT NULL DEFAULT 'BARU',
  `TCTitle` varchar(200) NOT NULL DEFAULT '',
  `TCDesc` text NOT NULL,
  `TCName` varchar(200) NOT NULL DEFAULT '',
  `TCEmail` varchar(200) NOT NULL DEFAULT '',
  `TCGender` varchar(10) DEFAULT '',
  `TCPhone` varchar(50) DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;

INSERT INTO `ticket` (`Uniq`, `TCNo`, `TCStatus`, `TCTitle`, `TCDesc`, `TCName`, `TCEmail`, `TCGender`, `TCPhone`, `CreatedOn`)
VALUES
	(3,'00003','BARU','Testing','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Rolas Simanjuntak','rolassimanjuntak@gmail.com',NULL,'085359867032','2021-06-14 00:18:28');

/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ticket_attachments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ticket_attachments`;

CREATE TABLE `ticket_attachments` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `TCId` bigint(10) unsigned DEFAULT NULL,
  `TCCommentId` bigint(10) DEFAULT NULL,
  `TCFileName` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`Uniq`),
  KEY `FK_TICKET_ATTC` (`TCId`),
  CONSTRAINT `FK_TICKET_ATTC` FOREIGN KEY (`TCId`) REFERENCES `ticket` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `ticket_attachments` WRITE;
/*!40000 ALTER TABLE `ticket_attachments` DISABLE KEYS */;

INSERT INTO `ticket_attachments` (`Uniq`, `TCId`, `TCCommentId`, `TCFileName`)
VALUES
	(3,3,NULL,'photo-1506744038136-46273834b3fb1.jpeg');

/*!40000 ALTER TABLE `ticket_attachments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ticket_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ticket_comments`;

CREATE TABLE `ticket_comments` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `TCId` bigint(10) unsigned NOT NULL,
  `TCComment` text NOT NULL,
  `CreatedBy` varchar(200) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_TICKET_COMMENT` (`TCId`),
  CONSTRAINT `FK_TICKET_COMMENT` FOREIGN KEY (`TCId`) REFERENCES `ticket` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table ticket_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ticket_log`;

CREATE TABLE `ticket_log` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `TCId` bigint(10) unsigned NOT NULL,
  `TCStatus` varchar(10) NOT NULL DEFAULT '',
  `CreatedBy` varchar(200) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_TICKET_COMMENT` (`TCId`),
  CONSTRAINT `FK_LOG_TICKET` FOREIGN KEY (`TCId`) REFERENCES `ticket` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
